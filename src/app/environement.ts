export const environment = {
  production: false,
  firebase:{
  	apiKey: "AIzaSyCphRskrm4UD09RCSaRiQBeUAih7NskID8",
    authDomain: "geohunt-afa96.firebaseapp.com",
    databaseURL: "https://geohunt-afa96.firebaseio.com",
    projectId: "geohunt-afa96",
    storageBucket: "geohunt-afa96.appspot.com",
    messagingSenderId: "492743358602"
  },
  mapbox: {
    accessToken: 'pk.eyJ1Ijoic2l0cmFrYXJhdiIsImEiOiJjanMwMWRpbTYxYTRzM3lvNWF4b3VqMGM3In0.GxdngUbBJ1yNOzVA98z79A'
  }
};