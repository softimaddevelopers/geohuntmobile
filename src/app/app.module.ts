import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { FaqPage } from '../pages/faq/faq';
import { TabsPage } from '../pages/tabs/tabs';
import { AdventureDetailPage } from '../pages/adventure-detail/adventure-detail';

import { QuizzPage } from "../pages/quizz/quizz";

import { ResultPage } from '../pages/result/result';
import { LeaderboardPage } from '../pages/leaderboard/leaderboard';
import  { GeolocalisationPage } from '../pages/geolocalisation/geolocalisation';
import { PhotoPage } from '../pages/photo/photo';
import { QrcodePage } from '../pages/qrcode/qrcode';
import { InfoPage } from '../pages/info/info';
// import { PuzzleThreePage } from '../pages/puzzle-three/puzzle-three';

import { ScanPage } from '../pages/scan/scan';
import { PaiementPage } from '../pages/paiement/paiement';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { PayPal } from '@ionic-native/paypal';
import { FileTransferObject,FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { PuzzlesPage } from '../pages/puzzles/puzzles';
import { CardInfoStripePage  } from '../pages/card-info-stripe/card-info-stripe';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { QRScanner } from '@ionic-native/qr-scanner';
import { Geolocation } from '@ionic-native/geolocation';
import { FilePath } from '@ionic-native/file-path';
import { MyHammerConfig } from '../pages/puzzles/HammerConf';


import { PaiementProvider } from '../providers/paiement/paiement';
import { environment } from './environement';


import { ComptageProvider } from "../providers/comptage/comptage";
import { AventureProvider } from '../providers/aventure/aventure';
import { HttpModule} from'@angular/http';
import { Stripe } from '@ionic-native/stripe';
import { NativeStorage } from '@ionic-native/native-storage';
import { NgProgressModule } from 'ngx-progressbar';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

import { Diagnostic } from '@ionic-native/diagnostic';
import { JeuxProvider } from '../providers/jeux/jeux';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';

import { Network } from '@ionic-native/network';

import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
//import { EmailComposer } from '@ionic-native/email-composer';

//import { NgxLoadingModule } from 'ngx-loading';

/*export const firebaseConfig = {

    apiKey: "AIzaSyCphRskrm4UD09RCSaRiQBeUAih7NskID8",
    authDomain: "geohunt-afa96.firebaseapp.com",
    databaseURL: "https://geohunt-afa96.firebaseio.com",
    projectId: "geohunt-afa96",
    storageBucket: "geohunt-afa96.appspot.com",
    messagingSenderId: "492743358602"

};*/

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    FaqPage,
    TabsPage,
    AdventureDetailPage,
    QuizzPage,

    ResultPage,
    LeaderboardPage,
    PuzzlesPage,
    CardInfoStripePage,
    GeolocalisationPage,
    PhotoPage,
    QrcodePage,
    ScanPage,
    PaiementPage,
    InfoPage,
    // PuzzleThreePage
  ],
  imports: [
    BrowserModule,
    NgProgressModule,
    IonicModule.forRoot(MyApp),
    AngularFireDatabaseModule,
    IonicStorageModule.forRoot(),
    //NgxLoadingModule.forRoot({}),
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebase),
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1Ijoic2l0cmFrYXJhdiIsImEiOiJjanMwMWRpbTYxYTRzM3lvNWF4b3VqMGM3In0.GxdngUbBJ1yNOzVA98z79A', // Optionnal, can also be set per map (accessToken input of mgl-map)
      // geocoderAccessToken: 'TOKEN' // Optionnal, specify if different from the map access token, can also be set per mgl-geocoder (accessToken input of mgl-geocoder)
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    FaqPage,
    TabsPage,
    AdventureDetailPage,

    QuizzPage,

    ResultPage,
    LeaderboardPage,

    PuzzlesPage,
    CardInfoStripePage,
    GeolocalisationPage,
    PhotoPage,
    QrcodePage,
    ScanPage,
    PaiementPage,
    InfoPage,
    // PuzzleThreePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    QRScanner,
    Geolocation,
    BackgroundGeolocation,
    FilePath,
    /*EmailComposerOriginal,*/
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HAMMER_GESTURE_CONFIG , useClass: MyHammerConfig},
    /*{provide: HAMMER_GESTURE_CONFIG , useClass: EmailComposer},*/
    ComptageProvider,
    PaiementProvider,
    PayPal,
    FilePath,
    FileTransferObject,
    FileTransfer,
    File,
    Diagnostic,
    Stripe,
    NativeStorage,
    AventureProvider,
    JeuxProvider,
    Network
  ]
})
export class AppModule {}
