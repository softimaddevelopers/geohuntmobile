import { Component } from '@angular/core';
import { /*IonicPage,*/ NavController, NavParams, ViewController,ModalController,LoadingController,AlertController } from 'ionic-angular';

import { AdventureDetailPage } from '../adventure-detail/adventure-detail';
import { HomePage } from '../home/home';
import {CardInfoStripePage} from '../card-info-stripe/card-info-stripe';
import  { PaiementProvider } from '../../providers/paiement/paiement';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { Http, Headers } from '@angular/http';
import { ComptageProvider } from '../../providers/comptage/comptage';
import { Stripe } from '@ionic-native/stripe';
import { NativeStorage } from '@ionic-native/native-storage';
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the PaiementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-paiement',
  templateUrl: 'paiement.html',
})
export class PaiementPage {
  public urlStripeServer:string ='https://geohuntserver.herokuapp.com/charge' ;
  public headers   : any    = new Headers({ 'Content-Type': 'application/json' });
  public prix_aventure:string='';
  public aventure:any;
  public loading = false;
  public jeux;
  public joueur_key;
  public paiement_key; 



  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
  	public viewCtrl: ViewController,
    public paiement: PaiementProvider,
    private payPal: PayPal ,
    public http:Http,
    public modalCtrl:ModalController,
    public cpt : ComptageProvider,
    private stripe: Stripe,
    public loadingCtrl: LoadingController,
    public translate: TranslateService,
    public alertCtrl:AlertController,
    private nativeStorage: NativeStorage
  	) {
    this.prix_aventure =this.navParams.get('prix_aventure');
    this.aventure = this.navParams.get('aventure');
    console.log('prix aventure', this.prix_aventure);

    console.log('id aventure', this.aventure)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaiementPage');
  }

  payerWithPaypal(){
    let key=this.paiement.create('/joueur').key;
    this.joueur_key = key;
    let aventure = {
      "fr" : "Aventure",
      "en" : "Game"
    }

    console.log("cléf joueur: "+key);

    // module paypale
    this.payPal.init({

    PayPalEnvironmentProduction: 'AXmYF-bNOA_jBbWfJWfOLharj7YmQXhrpWRFaI0yaLjO8H8vLDWVSylPqpVNRPn94pfZZGtexl9Mm2zU',
    PayPalEnvironmentSandbox: 'AXmYF-bNOA_jBbWfJWfOLharj7YmQXhrpWRFaI0yaLjO8H8vLDWVSylPqpVNRPn94pfZZGtexl9Mm2zU'
  }).then(() => {
    // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
    this.payPal.prepareToRender('PayPalEnvironmentProduction', new PayPalConfiguration({
      // Only needed if you get an "Internal Service Error" after PayPal login!
      //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
    })).then(() => {
      let payment = new PayPalPayment(this.prix_aventure, 'USD',aventure[this.translate.currentLang]+' - '+this.aventure.titre[this.translate.currentLang], 'sale');
      this.payPal.renderSinglePaymentUI(payment).then((data) => {

        // Successfully paid
        //alert('successfully payed');
        //alert('data: '+ JSON.stringify(data));

        let payement={
          method:'paypal',
          joueur_id:key,
          aventure_id:this.aventure.key,
          status:'payed',
          paypal_info: data,
          date_fin_aventure: Date.now(),
          date_deb_aventure:Date.now(),
          password:this.randomAlphaNum(10+Math.random()*10)
        }
        
        //payer
        this.payer(payement);

      }, (err) => {
        // Error or render dialog closed without being successful
        alert('error dialog with paypal'+JSON.stringify(err))
      });
    }, (err) => {
      // Error in configuration
      alert('config error'+JSON.stringify(err))
    });
  }, (err) => {
    // Error in initialization, maybe PayPal isn't supported or something else
    alert('initialization error'+ JSON.stringify(err))
  });
}

  cancel(){
  	this.viewCtrl.dismiss();
  	this.navCtrl.push(HomePage);
  }
  payerWithStripe(){
     let that = this;
     const cardmodal = this.modalCtrl.create(CardInfoStripePage,{prix_aventure:this.prix_aventure});
     cardmodal.onDidDismiss((card)=>{
        if(!card.quit){
          // Create the popup
          let loadingPopup = this.loadingCtrl.create({
            content: 'Payment Process...'
          });
          // Show the popup
          loadingPopup.present();
         
         this.stripe.setPublishableKey('pk_test_KuzcNWLeTxEhqWBDJuKsX5ID');
         this.stripe.createCardToken(card)
             .then((token) => {
                 //alert(token.id);
                 let key = this.paiement.create('/joueur').key;
                 this.joueur_key = key;
                 let data = {
                   stripetoken:token.id,
                   amount:Number(this.prix_aventure),
                   aventure:this.aventure
                 };
                 //alert(this.urlStripeServer);
                 //let self= this;
                 this.http.post(this.urlStripeServer,data, this.headers).subscribe((charge:any) =>{
                   /*if(charge.err){
                     alert(J);
                     loadingPopup.dismiss();
                   }*/
                    // alert(JSON.stringify(charge));
                     let payement={
                      method:'stripe',
                      joueur_id:key,
                      aventure_id:that.aventure.key,
                      status:'payed',
                      stripe_info:JSON.parse(charge._body).charge,
                      password:this.randomAlphaNum(10+Math.random()*10),
                      date_deb_aventure: Date.now(),
                      date_fin_aventure: Date.now()
                    }
                    this.payer(payement);
                    loadingPopup.dismiss();
                 },(err)=>{
                   alert(JSON.stringify(err));
                   loadingPopup.dismiss();
                 })
             })
             .catch((error) => {alert(error);loadingPopup.dismiss();});
        }
     });
     cardmodal.present();
  }

  payer(data:any){
    /*this.paiement.create('/paiement',payement);*/

    //alert(' function payer');
    //let datePaiment = new Date();
    // alert(JSON.stringify(data));
    this.viewCtrl.dismiss();
    // this.cpt.startTimer();
    this.navCtrl.push(AdventureDetailPage,{key_aventure:this.aventure.key, payed:true});
    this.loading = true;
    let paiment = this.paiement.create('/paiement',data);
    paiment.then((resp)=>{
    let message = "Paiement effectué avec succès";
      this.translate.get([message]).subscribe((value)=>{
         this.alert(value[message],'ok');
      })
    },(err)=>{
      alert("Error "+JSON.stringify(err));
    });
    this.nativeStorage.setItem('paiement'+this.aventure.key,{payed:true,aventureId:this.aventure.key,key_paiment:paiment.key, joueur_id:this.joueur_key})
                      .then((success)=>{ /*alert(success)*/},(err)=>{ alert('err function payer')});
    this.setStorage();
  }

  setStorage(){
    let joueurJeux ={};
    joueurJeux[this.aventure.key] = {};
    
    joueurJeux[this.aventure.key][this.joueur_key] = {};
    // alert("setting jouerJeux "+JSON.stringify(joueurJeux));
    this.nativeStorage.setItem('joueurJeux',joueurJeux).then((data)=>{
      // alert("Success "+JSON.stringify(data));

    },(err)=>{
      alert("Error "+JSON.stringify(err));
    });

  }
  alert(message:string,textButton:string,classAlert:string=''){
    this.translate.get([message,textButton]).subscribe((value)=>{
        const prompt = this.alertCtrl.create({
        message: value[message] ,
        buttons: [
            {
              text: value[textButton] ,
              handler: data => {

              }
            }
        ],
        cssClass:classAlert
        });
        prompt.present();
    })
  }
  randomAlphaNum(length=4){
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let retour='';
    for (var i=0;i<length;i++){
      retour+=possible[Math.floor(Math.random()*possible.length)];
    }
    return retour;
  }
}
