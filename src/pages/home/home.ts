import { Component } from '@angular/core';
import { NavController, LoadingController,Platform , Events} from 'ionic-angular';
import { AdventureDetailPage } from '../adventure-detail/adventure-detail';
import { AventureProvider } from '../../providers/aventure/aventure';
import { Aventure } from "../../models/aventure-model";
import { TranslateService } from '@ngx-translate/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { AngularFireStorage } from 'angularfire2/storage';

import { AngularFireDatabase } from 'angularfire2/database';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	pathData:string='/aventures';
	titre:string;
	description:string;
	temps_min:number;
	temps_max:number;
	niveau:string;
	idAventures:string;
	test:string = "balbala";
	avatar:Blob;
	imageSrc:any;
	aventure: any;
	aventures: Aventure[];
	plus:boolean =false;
	loading:boolean=false;// afficher loader
	key_aventure:Array<string> = [];

	fileTransfer: FileTransferObject = this.transfer.create();

	constructor(public navCtrl: NavController, 
		public adv : AventureProvider,
		public translate: TranslateService,
		public loadingCtrl: LoadingController,
		private nativeStorage: NativeStorage,
		private platform:Platform,
		 public events: Events,
		 private storage: AngularFireStorage,
		private db:AngularFireDatabase,
		private file: File,
		private transfer: FileTransfer
		) {
	}

	ionViewDidEnter(){
		this.events.publish('language', {fr: true, en: true});

	}
	ionViewDidLoad(){
		
		let that = this;
		let chargement = "Chargement en cours...";
					this.translate.get([chargement]).subscribe((value)=>{
			    	let loading = this.loadingCtrl.create({
				  		content: value[chargement]
				  	});

				  	loading.present();	


	

		if(!navigator.onLine){
			this.nativeStorage.getItem("aventures").then((value) =>{
				this.aventures = JSON.parse(value);
				this.aventures.forEach((aventure) =>{
	
				      let img = aventure.avatar_local_url;
				      
				      let correctPath = img.substr(0, img.lastIndexOf('/') + 1); 
				      let currentName = img.substring(img.lastIndexOf('/')+1);
			
				      this.file.readAsDataURL(correctPath, currentName).then( dataurl =>{
				                aventure.avatar_url = dataurl;
				              });
				    
				})
				loading.dismiss();

			})
		}

		this.db.database.ref("/aventures").on("value", (data) => {
			this.aventures = [];
			if(!window.cordova){
				alert("web");
				data.forEach((dataChild) =>{
					let aventure = new Aventure().deserialize(dataChild.val());
					this.aventures.push(aventure);
				})

							loading.dismiss();
			

			}else{
				let promises = [];
				data.forEach((dataChild) =>{
								let aventure = new Aventure().deserialize(dataChild.val());
								if(aventure.status == "valide"){
									let ext = this.fileExtension(aventure.avatar_url);
									let imageLocal = this.file.dataDirectory+ aventure.key+ '/avatar.' + ext;
	                                

	                           		promises.push(this.fileTransfer.download(aventure.avatar_url, imageLocal));
	                           		aventure.avatar_local_url = imageLocal;
	                           		
      
	                           		this.aventures.push(aventure);
								}
									
				})
				this.nativeStorage.setItem('aventures',JSON.stringify(this.aventures)).then(() =>{
								loading.dismiss();
								Promise.all(promises).then(() =>{
									
									
								}).catch((err) =>{
									// alert("Download error : "+ JSON.stringify(err));
								})
								
				});
			}
				
			
				
		}, (err) =>{
				// this.nativeStorage.getItem('aventure').then((data) =>{
				// 				that.aventure = data;
				// 				if(that.key_aventure.length == 0){
				// 					for(let aventure in that.aventure){
				// 						if(that.aventure[aventure].status == "valide"){
				// 							that.key_aventure.push(aventure);
				// 						}
										
				// 					}
				// 				}
								  	
				// 				console.log("av valide %o", that.key_aventure);
				// 				loading.dismiss();
				// });
		});
			
				
			
				
		
		})
	}
	selectAdventure(aventure:Aventure){
	  	this.navCtrl.push(AdventureDetailPage,{"key_aventure":aventure.key, "aventure": JSON.stringify(aventure)});
	}

	isHidden(aventure:Aventure){
		for(let i in aventure.language){
			if(aventure.language[i] && i == this.translate.currentLang){
				return false;
			}
		}

		if(aventure.status == "valide"){
			return false;
		}
		return true;
	}

	fileExtension( url ) {
	    return url.split('.').pop().split(/\#|\?/)[0];
	}
   
}
