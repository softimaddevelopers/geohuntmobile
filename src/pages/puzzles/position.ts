interface position{
	x:number;
	y:number;	
	label:string;
	style:Object;
	xOriginal:number;
	yOriginal:number;
}

export class Piece implements position {
	x:number;
	y:number;
	label:string;
	style:Object;
	xOriginal:number;
	yOriginal:number;
	constructor(x:number,y:number,label:string,style:Object,xOriginal:number,
	yOriginal:number) {
		// code...
		this.x=x;
		this.y=y;
		this.label =label;
		this.style=style;
		this.xOriginal = xOriginal;
		this.yOriginal = yOriginal;
	}
	
	getStyle(){
		return this.style;
	}

}