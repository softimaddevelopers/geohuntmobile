import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';

import { Piece } from './position';

import { PhotoPage } from "../photo/photo";
import { ComptageProvider } from '../../providers/comptage/comptage';
import { QuizzPage } from '../quizz/quizz';
//import { PhotoPage} from '../photo/photo';
import { GeolocalisationPage } from '../geolocalisation/geolocalisation';
import { QrcodePage } from '../qrcode/qrcode';
import { LeaderboardPage } from '../leaderboard/leaderboard';

import { NativeStorage } from '@ionic-native/native-storage';
import { TranslateService } from '@ngx-translate/core';
import { File } from '@ionic-native/file';
import { Jeux } from '../../models/jeux-model';
import { InfoPage } from '../info/info';
/**
 * Generated class for the PuzzlesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-puzzles',
  templateUrl: 'puzzles.html',
})
export class PuzzlesPage {
  pieces:Array<Piece>=[];
  vide=new Piece(3,3,'',{
    'background-color':'white'
  },
  3,3
  );
  rapport:string = '';
  key_aventure:string;
  jeu:Jeux = new Jeux();
  jeux:Jeux[];
  jeuSuivant:Jeux;
  gameReady:boolean=false;
  canNext:boolean=false;

  etat:any = false;
  joueur_key:any;

  show:Boolean = false;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public nativeStorage: NativeStorage,
    public cpt : ComptageProvider,

    public translate:TranslateService,
    public alertCtrl: AlertController,
    private file:File
    ) {
    //dynamique

    

  }

  ionViewDidLoad() {

    this.joueur_key = this.navParams.get("joueur_key");

    this.jeu= new Jeux().deserialize(this.navParams.get("ceJeu"));
    this.jeux= this.navParams.get("jeux").map((a:Jeux) => new Jeux().deserialize(a));
    this.nativeStorage.setItem('dernierSequence'+this.jeu.aventures_id,{sequence:this.jeu.sequence});
    this.jeux = this.jeux.sort(function(a:Jeux, b:Jeux){return Number(b.sequence) -Number(a.sequence)});
    let curSequence:Number =  0;

    let self = this;
    for(var i=0;i<this.jeux.length;i++){
        curSequence = this.jeux[i].sequence;
  
        if(curSequence <= Number(this.jeu.sequence)) {
          this.jeuSuivant = (i!=0) ? this.jeux[i-1] : new Jeux();
          break;
        }else {
          this.jeuSuivant = new Jeux();
        }
    }
    console.log('jeu:',this.jeu)
    console.log('jeux:',this.jeux)
    console.log('jeuxSuivant:',this.jeuSuivant);

    if(this.jeu.image_url){
      let img = this.jeu.image_url;
      
      let correctPath = img.substr(0, img.lastIndexOf('/') + 1); 
      let currentName = img.substring(img.lastIndexOf('/')+1);
      this.file.readAsDataURL(correctPath, currentName).then( dataurl =>{
                this.jeu.image_url = dataurl;
                let counter =0;
                for (var i=0;i<4;i++){
                  for(var j=0;j<4;j++){
                    counter++;
                    let piece= new Piece(i,j,''+counter,{
                      'background-image':'url(\''+this.jeu.image_url+'\')', /*../../assets/imgs/puzzle.jpg*/
                      'background-repeat':'no-repeat',
                      'background-position':-65*j+'px '+65*(-i)+'px' 
                    },
                    i,j
                  );
                    this.pieces.push(piece);  
                  }
                }
                this.pieces.splice(15,1,this.vide);
                this.gameReady=false;
                this.melanger();

                
                setTimeout(function() {
                  self.show = true;

                }.bind(this), 1000);

              }).catch(function(err){
                alert('Error '+JSON.stringify(err));
      })
    }
   
                

    this.nativeStorage.setItem('indicesPenalisations'+this.jeu.aventures_id+this.joueur_key+this.jeu.key,
      {indices: this.jeu.indices,numero:0}).then((success)=>{
       console.log("indice sauvé")
    },(err)=>{ 
      console.log('indice non sauvé'+err)
    });


    
  }



  melanger(){
    let nombreDeRandom:number = 500;
    for (var j=0;j<=nombreDeRandom;j++){
        let pieces:Array<Piece>=[];
        for(var i = 0;i<this.pieces.length;i++){
          if (Math.abs(this.pieces[i].x-this.vide.x)+Math.abs(this.pieces[i].y-this.vide.y)<=1){
            pieces.push(this.pieces[i]);
          }
        }
        let randomNumber = Math.floor(pieces.length*Math.random());
        if(pieces[randomNumber]!=undefined){
          this.deplace(pieces[randomNumber]);
        }
        else{
          this.deplace(pieces[randomNumber-1]);
        }
    }
    this.gameReady= true;
  }

  deplace(piece){
    let x:number;
    let y:number;
    if (Math.abs(piece.x-this.vide.x)+Math.abs(piece.y-this.vide.y)<=1){
      
      let indexVide = this.pieces.indexOf(this.vide);
      let indexPiece = this.pieces.indexOf(piece);
      let echange = this.pieces.splice(indexVide,1,piece);
      this.pieces.splice(indexPiece,1,echange[0]);
      x =piece.x;
      y =piece.y;
      piece.x =this.vide.x;
      this.vide.x =x;
      piece.y = this.vide.y;
      this.vide.y=y;
    }
    this.setRapport();
    console.log(piece);
    console.log(this.vide);
  }

  setRapport(){
    let gain:number=0;
    for(var i=0;i<this.pieces.length;i++){
      if(this.pieces[i].x == this.pieces[i].xOriginal && this.pieces[i].y == this.pieces[i].yOriginal){
        gain++;
      }
    }
    if(gain>1){
      let rapport= gain+' pièces parmis 16 sont sur les bonnes places';
      this.translate.get([rapport]).subscribe((value)=>{
        this.rapport = value[rapport];
      })
    }
    if(gain ==16 && this.gameReady){
      this.canNext = true;
    }
  }

  skipGame(){

    let cancel = "ANNULER";
    let accept = "ACCEPTER";
    let warning = "Des pénalisations vous seront remis si vous passer directement au suivant";
    let penal_indice = "Temps pénalisé : ";
    this.translate.get([cancel, accept, warning]).subscribe(
      value => {
      let confirm = this.alertCtrl.create({
        title: value[warning]+' : <br>'+this.jeu.penalisation_passer+" s",
        buttons: [
        {
          text: value[cancel],
          handler: () => {

          }
        },
        {
          text: value[accept],
          handler: () => {
            this.etat = true; 
            this.nextGame();

          }
        }
        ],
        cssClass: 'alert-geohunt'
      });
      confirm.present(); 
    })
  }

  indiceJeu(){

    this.nativeStorage.getItem('indicesPenalisations'+this.jeu.aventures_id+this.joueur_key+this.jeu.key).then((data)=>{
      console.log("les data venant de storage indice penalisation: "+data);
      let indices = data.indices;
      let numero = data.numero;
      console.log(numero);

      //this.cpt.penalisation_indices(indices,numero,this.jeu.aventures_id,this.joueur_key,this.jeu.key);

    });

  }

  nextGame(){

    if(this.etat == true){
      this.cpt.endTimerJeu(this.jeu.type,this.jeu.sequence,this.jeu.key,this.jeu.aventures_id,this.jeu.penalisation_passer,true);
      this.etat = false;
    } else {
      this.cpt.endTimerJeu(this.jeu.type,this.jeu.sequence,this.jeu.key,this.jeu.aventures_id,this.jeu.penalisation_passer,false);
    }
    switch (this.jeuSuivant.type) {
     case "quizz":
        // code...
        this.navCtrl.push(QuizzPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "puzzle":
        // code...
        this.navCtrl.push(PuzzlesPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "geolocalisation":
        // code...
        this.navCtrl.push(GeolocalisationPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "qrcode":
        // code...
        this.navCtrl.push(QrcodePage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "photo":
        // code...
        this.navCtrl.push(PhotoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "info":
        // code...
        this.navCtrl.push(InfoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      default:
        // code...
        this.navCtrl.push(LeaderboardPage,{id_aventure:this.jeu.aventures_id,dernierJeu:this.jeu});
        break;
    }

    //this.cpt.endTimerJeu("puzzle",4);

    // this.navCtrl.push(PhotoPage);

  }

}
