import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AventureProvider } from '../../providers/aventure/aventure';
import { TranslateService } from '@ngx-translate/core';
import { Lang } from "../../models/lang-model";

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

	termes:Lang = new Lang();

  constructor(public navCtrl: NavController, 
    public adv : AventureProvider,
    public translate: TranslateService
    ) {

  	this.setTermes();
  	
  }

  setTermes(){

    let that = this;
    this.adv.get("/contact").snapshotChanges().subscribe((data) => {
      that.termes = new Lang().deserialize(data.payload.val());
 		});

  }

}
