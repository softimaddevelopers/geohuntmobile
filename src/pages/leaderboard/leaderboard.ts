import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ComptageProvider } from '../../providers/comptage/comptage';
import { NativeStorage } from '@ionic-native/native-storage';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';
import { Jeux } from '../../models/jeux-model';
import { File } from '@ionic-native/file';

/**
 * Generated class for the LeaderboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-leaderboard',
  templateUrl: 'leaderboard.html',
})
export class LeaderboardPage {
  joueur_id:any;
  aventure_id:any;
  dernierJeux:any;
  joueur:any = {};
  joueurs:any=[];
  joueurJeux:any=[];
  jeux:Jeux[];
  joueurJeuxClone:any=[];
  view:Boolean=false;
  temps_avMax:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public cpt : ComptageProvider,
    public db:AngularFireDatabase,
    private nativeStorage: NativeStorage,
    public loadingCtrl: LoadingController,
    public storage: AngularFireStorage,
    public file: File
    ) {
    
  }

  /*ionViewWillLeave() {
    this.cpt.startTimer();
  }*/
  loadJoueurJeux(byWhat='temps_total'){
      let self = this;
      this.db.object('joueur').snapshotChanges().subscribe((data)=>{
         self.joueurs = data.payload.val();
         //console.log('joueurs',self.joueurs);
       });
      this.db.object("joueurJeux/"+this.aventure_id).snapshotChanges().subscribe((data)=>{
          let labels=[];
          for(var key in data.payload.val()){
            ///alert('a');
            let clone:any= Object.assign({},data.payload.val()[key])
            clone.key = key;
            labels.push(key);
            self.joueurJeuxClone.push(clone);
          }
          console.log('hanao trie');
          self.tri(self.joueurJeuxClone,byWhat);
          self.joueurJeuxClone = self.joueurJeuxClone.slice().reverse();
          self.joueurJeux = self.joueurJeuxClone;
          self.joueurJeuxClone=[];
          /*console.log('avy ntriena reverse');
          console.log(self.joueurJeuxClone.slice().reverse());*/
          
      })
  }


  tri(dataArray:any=[],byTemps_total='temps_total',n:number=null){
    if(n==null) n = dataArray.length;
    if(n==0){
      return dataArray;
    }
    else{
      let temp_max=0;
      let toShiftData=null;
      for(var i=0; i<n;i++){
        if(dataArray[i][byTemps_total] > temp_max){
          temp_max = dataArray[i][byTemps_total]
          toShiftData= dataArray[i];
        }
      }
      toShiftData = dataArray.splice(dataArray.indexOf(toShiftData),1);
      dataArray.push(toShiftData[0]);
      return this.tri(dataArray,byTemps_total,n-1);
    }
    //console.log(dataArray);
  }




  ionViewDidLoad() {
    this.view = this.navParams.get('result');

    this.aventure_id= this.navParams.get("id_aventure");
    this.dernierJeux = this.navParams.get("dernierJeu");

    this.db.object('aventures/'+this.aventure_id+"/temps_max").snapshotChanges().subscribe((data)=>{
      this.temps_avMax = data.payload.val();

    });

    this.nativeStorage.getItem("temps_total_aventure"+this.aventure_id).then((data:string) =>{
       // if(data.split(':').length > 0){
         self.cpt.pause = data;
       // }
       
     });

     this.nativeStorage.getItem('paiement'+this.aventure_id).then( (paie:any) =>{
       
         self.joueur_id = paie.joueur_id;
       
     });

    this.loadJoueurJeux('rang');

    if(!this.view){
      this.nativeStorage.setItem('dernierSequence'+this.aventure_id,{sequence:this.dernierJeux.sequence+1});
      this.jeux= this.navParams.get("jeux").map((a:Jeux) => new Jeux().deserialize(a));
      for(let i in this.jeux){
        this.aventure_id = this.jeux[i].aventures_id;
        break;
      }
    }
    

    this.loadJoueurJeux();

    this.cpt.finTimerJeu();

   
  
    
 
    let self = this;
    console.log('ionViewDidLoad LeaderboardPage');
     this.nativeStorage.getItem('paiement'+this.aventure_id).then( (paie:any) =>{
         this.db.database.ref("joueur/"+paie.joueur_id).on("value", function(snapshot){
           if(snapshot.val()){
             self.joueur = snapshot.val();
           }
         })
     })
    
  }

  send(){

    if(this.joueur.firstname && this.joueur.lastname){
        const loader = this.loadingCtrl.create({
          content: "Loading...",
        });
        let self = this;
        loader.present();

        this.nativeStorage.getItem('joueurJeux').then((data:any)=>{   
        // alert("storage joueurJeux")          
              this.nativeStorage.getItem('paiement'+this.aventure_id).then( (paie:any) =>{
                // alert("paiement")
                   this.nativeStorage.setItem('leaderboard'+paie.aventureId, true);

                   this.joueur_id = paie.joueur_id;

                   this.nativeStorage.getItem("joueur").then((joueur)=>{
                       joueur.firstname = this.joueur.firstname;
                       joueur.lastname = this.joueur.lastname;


                       this.db.database.ref("joueur/"+paie.joueur_id).set(joueur).then(()=>{
                             
                           // alert("joueur");
                           // alert("AVENTUREiD");
                           // alert(paie.aventureId);
                           // alert(JSON.stringify(data));
                           // alert(JSON.stringify(data[paie.aventureId]));
                           // alert(JSON.stringify(data[paie.aventureId][paie.joueur_id]));
                             this.db.database.ref("joueurJeux/"+paie.aventureId+"/"+paie.joueur_id).set(data[paie.aventureId][paie.joueur_id]).then(()=>{
                               // alert("joueurJeux");

                               let wait_photo_upload = false;
                              // alert(JSON.stringify(data[self.aventure_id][paie.joueur_id]));
                              // alert(JSON.stringify(data[self.aventure_id][paie.joueur_id].jeux));
                              for(let i in data[self.aventure_id][paie.joueur_id].jeux){
                               
                                if(data[self.aventure_id][paie.joueur_id].jeux[i].type_jeu == "photo"){
                                      // alert("photo type");
                                       
                                        wait_photo_upload = true;
                                        
                                        this.nativeStorage.getItem("joueurJeuxPhoto"+paie.aventureId+i).then((photo)=>{
                                          
                                          let img = photo;
                
                                          let correctPath = img.substr(0, img.lastIndexOf('/') + 1); 
                                          let currentName = img.substring(img.lastIndexOf('/')+1);
                                         
                                          this.file.readAsDataURL(correctPath, currentName).then( dataurl =>{
                                                    
                                                      self.storage.ref("joueurJeuxPhoto/"+paie.aventureId+"/"+paie.joueur_id+"/"+i+"/photo").putString(dataurl, 'data_url').then(function(storageSnap){
                                                         
                                                          loader.dismiss();
                                                          storageSnap.ref.getDownloadURL().then(value =>{
                                                              self.db.database.ref("joueurJeuxPhoto/"+paie.aventureId+"/"+paie.joueur_id+"/"+i).set(value).then(()=>{
                                                                    
                                                                    self.view = true;
                                                                    self.loadJoueurJeux()
                                                              })
                                                          });
                                                              
                                                      }, (err) =>{
                                                          alert('Error '+ JSON.stringify(err));
                                                      });

                                                  }).catch(function(err){
                                                    alert('Error '+JSON.stringify(err));
                                          });

                                                      
                                              
                                        }, (err) =>{

                                            loader.dismiss();
                                            self.view = true;
                                            self.loadJoueurJeux()
                                        });


                                }
                              }
                              if(!wait_photo_upload){
                                  self.view = true;
                                  self.loadJoueurJeux()
                              }
                                        
                             
                           }, (err) =>{
                             alert('Error '+ JSON.stringify(err));
                           })
                       })  
                   })

                           
              })
             
            });
    }else{
      alert("Fill firstname and lastname")
    }
        
  }

  public getSecond(nb){
    let sec = nb.split(":");
    let sec_tot = ((nb[0]*3600)+(nb[1]*60)+(nb[2]*60));
    return sec_tot;
  }  


}
