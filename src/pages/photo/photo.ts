import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams, Platform, normalizeURL, AlertController, LoadingController } from 'ionic-angular';

import  { QrcodePage} from "../qrcode/qrcode";
import { PuzzlesPage } from '../puzzles/puzzles';
import { QuizzPage } from '../quizz/quizz';
import { GeolocalisationPage } from '../geolocalisation/geolocalisation';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { ComptageProvider } from '../../providers/comptage/comptage';
import { LeaderboardPage } from '../leaderboard/leaderboard';
import { NativeStorage } from '@ionic-native/native-storage';
import { Diagnostic } from '@ionic-native/diagnostic';
import { File } from '@ionic-native/file';
import { TranslateService } from '@ngx-translate/core';

import { Jeux } from '../../models/jeux-model';
import { InfoPage } from '../info/info';

/**
 * Generated class for the PhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-photo',
  templateUrl: 'photo.html',
})
export class PhotoPage {
	options: CameraOptions = {
	  quality: 100,
    targetWidth: 600,
    targetHeight: 600,
	  destinationType: this.camera.DestinationType.FILE_URI,
	  encodingType: this.camera.EncodingType.JPEG,
	  mediaType: this.camera.MediaType.PICTURE
	};
  photo:any;
  photoUrl:any;
	imageName:string;
  key_aventure:string;

  jeu: Jeux = new Jeux();
  jeux:Jeux[];
  jeuSuivant:Jeux;


  etat:any = false;
  joueur_key:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public platform: Platform,
  	private camera: Camera,
  	private filePath: FilePath,
    private nativeStorage: NativeStorage,
    public cpt : ComptageProvider,
    public diagnostic: Diagnostic,
    public file: File,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public loadingCtrl: LoadingController

  	) {

  }

 

  ionViewDidEnter() {

    this.jeu= new Jeux().deserialize(this.navParams.get("ceJeu"));
    this.jeux= this.navParams.get("jeux").map((a:Jeux) => new Jeux().deserialize(a));
    this.nativeStorage.setItem('dernierSequence'+this.jeu.aventures_id,{sequence:this.jeu.sequence});
    this.jeux = this.jeux.sort(function(a:Jeux, b:Jeux){return Number(b.sequence) -Number(a.sequence)});
    let curSequence:Number =  0
    for(var i=0;i<this.jeux.length;i++){
        curSequence = this.jeux[i].sequence;
  
        if(curSequence <= Number(this.jeu.sequence)) {
          this.jeuSuivant = (i!=0) ? this.jeux[i-1] : new Jeux();
          break;
        }else {
          this.jeuSuivant = new Jeux();
        }
    }
    // alert(this.jeu.image_url);
    console.log('jeu:',this.jeu)
    console.log('jeux:',this.jeux)

    console.log('jeuxSuivant:',this.jeuSuivant);
    if(this.jeu.image_url){
      let img = this.jeu.image_url;
      
      let correctPath = img.substr(0, img.lastIndexOf('/') + 1); 
      let currentName = img.substring(img.lastIndexOf('/')+1);
      // alert(currentName);
      // alert(correctPath);
      this.file.readAsDataURL(correctPath, currentName).then( dataurl =>{
                this.jeu.image_url = dataurl;
              }).catch(function(err){
                alert('Error '+JSON.stringify(err));
      })
    }


    

  }

 
  takePicture(){
  	let self = this;
  	this.camera.getPicture(this.options).then((imagePath) => {
        // Special handling for Android library
        self.diagnostic.requestExternalStorageAuthorization().then(function(){
          self.diagnostic.isExternalStorageAuthorized().then(function(isAvailable){
            
          },(error)=>{
            alert('Permission denied.');
          })
        })
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(imagePath)
            .then(filePath => {
                
              this.photoUrl = filePath;
              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1); 
              let currentName = imagePath.substring(imagePath.lastIndexOf('/')+1);
               // alert(currentName);
              this.file.readAsDataURL(correctPath, currentName).then( dataurl =>{
                this.photo = dataurl;
              }).catch(function(err){
                alert('Error '+JSON.stringify(err));
              })

            });
        } else {
          this.photoUrl = imagePath;
          //var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1); importer non utiliser
          //var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          
        }
      }, (err) => {
        alert('Error while selecting image.'+ err);
      });

  }

  dataURLtoBlob(dataurl) {
    let  arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
  }

  createFileName(){
    let d = new Date();
    let n = d.getTime();
    let newFileName = n + ".jpg";
    return newFileName;
  }

  copyFileToLocalDir(namePath, currentName){
    //let self = this;
    return this.file.copyFile(namePath, currentName, this.getDataDir(),currentName ).then((success)=>{
    }, (error) =>{
          alert('Error when storing file ' +JSON.stringify(error));
    })
  }

  pathForImage(img){
    if(img){
      return this.getDataDir() +img;
    }else{
      return '';
    }
  }

  getDataDir(){
    if(this.platform.is('ios')){
      return this.file.dataDirectory;
    }else{
      return this.file.externalDataDirectory;
    }
  }

  skipGame(){

    let cancel = "ANNULER";
    let accept = "ACCEPTER";
    let warning = "Des pénalisations vous seront remis si vous passer directement au suivant";
    let penal_indice = "Temps pénalisé : ";
    this.translate.get([cancel, accept, warning]).subscribe(
      value => {
      let confirm = this.alertCtrl.create({
        title: value[warning]+' : <br>'+this.jeu.penalisation_passer+" s",
        buttons: [
        {
          text: value[cancel],
          handler: () => {

          }
        },
        {
          text: value[accept],
          handler: () => {
            this.etat = true; 
            this.nextGame();

          }
        }
        ],
        cssClass: 'alert-geohunt'
      });
      confirm.present(); 
    })
  }

  storeImage(){
   
    if(this.photo){
         const loader = this.loadingCtrl.create({
            content: "Loading...",
          });
          loader.present();
        this.nativeStorage.getItem('joueurJeux').then((data:any)=>{
         
          this.nativeStorage.getItem('paiement'+this.jeu.aventures_id).then( (paie:any) =>{

              
               // data[''+this.jeu.aventures_id][''+paie.joueur_id].jeux = {};
               // data[''+this.jeu.aventures_id][''+paie.joueur_id].jeux[this.jeu.key] = data[''+this.jeu.aventures_id][''+paie.joueur_id].jeux[this.jeu.key] ? data[''+this.jeu.aventures_id][''+paie.joueur_id].jeux[this.jeu.key] : {};
               //  data[''+this.jeu.aventures_id][''+paie.joueur_id].jeux[this.jeu.key].photo = this.photo;
               // alert(this.photoUrl);
               this.nativeStorage.setItem('joueurJeuxPhoto'+this.jeu.aventures_id+this.jeu.key, this.photoUrl).then(()=>{
                 loader.dismiss();
                 this.nextGame();
               },(err)=>{
                 alert('Err :'+err)
               });
          })
         
        });
    }
  }

  indiceJeu(){
    if(this.jeu.indices.length > 0){
      this.cpt.penalisation_indices(this.jeu,this.jeu.aventures_id,this.jeu.key);
    }
  }

  nextGame(){

    if(this.etat == true){
      this.cpt.endTimerJeu(this.jeu.type,this.jeu.sequence,this.jeu.key,this.jeu.aventures_id,this.jeu.penalisation_passer,true);
      this.etat = false;
    } else {
      this.cpt.endTimerJeu(this.jeu.type,this.jeu.sequence,this.jeu.key,this.jeu.aventures_id,this.jeu.penalisation_passer,false);
    }
  	switch (this.jeuSuivant.type) {
     case "quizz":
        // code...
        this.navCtrl.push(QuizzPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "puzzle":
        // code...
        this.navCtrl.push(PuzzlesPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "geolocalisation":
        // code...
        this.navCtrl.push(GeolocalisationPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "qrcode":
        // code...
        this.navCtrl.push(QrcodePage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "photo":
        // code...
        this.navCtrl.push(PhotoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "info":
        // code...
        this.navCtrl.push(InfoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      default:
        // code...
        this.navCtrl.push(LeaderboardPage,{id_aventure:this.jeu.aventures_id,dernierJeu:this.jeu});
        break;
    }
    //this.cpt.endTimerJeu("puzzle",3);

  }

}
