import { Component ,ViewChild,ElementRef} from '@angular/core';
import { NavController, NavParams, AlertController, ViewController} from 'ionic-angular';
import { ComptageProvider } from '../../providers/comptage/comptage';

import { PuzzlesPage } from '../puzzles/puzzles';
import { QuizzPage } from '../quizz/quizz';
import { PhotoPage} from '../photo/photo';
import { QrcodePage } from '../qrcode/qrcode';
import { LeaderboardPage } from '../leaderboard/leaderboard';
import { TranslateService } from '@ngx-translate/core';

import { NativeStorage } from '@ionic-native/native-storage';
import { Geolocation } from '@ionic-native/geolocation';
import { Jeux } from '../../models/jeux-model';
import { Diagnostic } from '@ionic-native/diagnostic';
import { DomSanitizer } from '@angular/platform-browser';
import { File } from '@ionic-native/file';
import { InfoPage } from '../info/info';
//import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
/**
 * Generated class for the GeolocalisationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-geolocalisation',
  templateUrl: 'geolocalisation.html',
})
export class GeolocalisationPage {
	@ViewChild('map') mapContainer: ElementRef;
  map: any;
  // key_aventure:string;
  jeu:Jeux = new Jeux();
  jeux:Jeux[];
  jeuSuivant:Jeux;

  geolatitude:any=24;
  geolongitude:any=24;
  scale= 14;
  maplatitude:any;
  maplongitude:any;


  joueur_key:any;
  etat:any = false;

  //mobilisation:any;
  subscription:any;

  oldMap:any;
  now:any;
  googleMap:any;
  listes_indices:any;
  nombre_indices:number;
  numero:number;
  nb_indices;

  coord:any[];
  myPosition:any[];
  constructor(
    public navCtrl: NavController, 
    public viewCtrl: ViewController, 
    public navParams: NavParams,
    private nativeStorage: NativeStorage,
    public geolocation: Geolocation,
    // private backgroundGeolocation: BackgroundGeolocation,
    public cpt : ComptageProvider,
    private diagnostic: Diagnostic,
    public translate:TranslateService,
    public alertCtrl: AlertController,
    private sanitizer:DomSanitizer,
    private file:File
    ) {


  }

  ionViewDidEnter() {
    // this.key_aventure=this.navParams.get("key_aventure");

    this.jeu= new Jeux().deserialize(this.navParams.get("ceJeu"));
    this.jeux= this.navParams.get("jeux").map((a:Jeux) => new Jeux().deserialize(a));
    this.nativeStorage.setItem('dernierSequence'+this.jeu.aventures_id,{sequence:this.jeu.sequence});
    this.googleMap = this.sanitizer.bypassSecurityTrustUrl("geo:0,0?q="+this.jeu.latitude+","+this.jeu.longitude);
    this.jeux = this.jeux.sort(function(a:Jeux, b:Jeux){return Number(b.sequence) -Number(a.sequence)});
    let curSequence:Number =  0;
    this.coord = [this.jeu.longitude, this.jeu.latitude];

    console.log("coord : %o", this.coord);
    for(var i=0;i<this.jeux.length;i++){
        curSequence = this.jeux[i].sequence;
  
        if(curSequence <= Number(this.jeu.sequence)) {
          this.jeuSuivant = (i!=0) ? this.jeux[i-1] : new Jeux();
          break;
        }else {
          this.jeuSuivant = new Jeux();
        }
    }

    if(this.jeu.image_url){
      let img = this.jeu.image_url;
      
      let correctPath = img.substr(0, img.lastIndexOf('/') + 1); 
      let currentName = img.substring(img.lastIndexOf('/')+1);
      this.file.readAsDataURL(correctPath, currentName).then( dataurl =>{
                this.jeu.image_url = dataurl;
              }).catch(function(err){
                alert('Error '+JSON.stringify(err));
      })
    }
 
    this.checkLocation();

    this.nombre_indices = this.jeu.indices.length;

    this.nativeStorage.getItem("paiement"+this.jeu.aventures_id).then((paie)=>{
      this.joueur_key = paie.joueur_id;
    });
  }



  locateMeNow(){
    
    this.geolocation.getCurrentPosition().then((response)=>{
      this.geolatitude = response.coords.latitude;
      this.geolongitude= response.coords.longitude;
    })
  }

  // loadGeoloc(){

  //   this.subscription = this.geolocation.watchPosition()
  //   .filter((p) => p.coords !== undefined)
  //   .subscribe((response)=>{
  //     this.geolatitude = response.coords.latitude;
  //     this.geolongitude= response.coords.longitude;
  //     this.myposition = leaflet.circle([this.geolatitude, this.geolongitude], {radius: 2}).addTo(this.map);
  //     this.myposition = leaflet.circle([this.geolatitude, this.geolongitude], {
  //         color: 'red',
  //         fillColor: '#f03',
  //         fillOpacity: 0.5,
  //         radius: 15
  //     }).addTo(this.map);
  //   })

  // }

  checkLocation() {
    //this.loadGeoloc();
    let self = this;
    // setTimeout(() => {
    self.loadMap(); 
    // }, 3000);
    
    this.diagnostic.requestLocationAuthorization().then(()=>{
        this.diagnostic.isLocationEnabled().then((isEnabled) =>{
          
          
          if(!isEnabled){

                self.diagnostic.switchToLocationSettings();
                
          }
        })
        
                
    });
  }

  ionViewDidLeave() {
        // alert("remove");
      // leaflet.map("map").remove();
      // let index = this.viewCtrl.index;
      // this.navCtrl.remove(index);
  }


    loadMap(){
                let that= this;
                // alert(JSON.stringify(this.map)); 
                // document.getElementById('map').innerHTML = "<div id='map' style='width: 100%; height: 100%;'></div>";
                // if(leaflet.map("map")) {
                //   leaflet.map("map").remove();
                // }

                // this.map = leaflet.map("map").fitWorld();
                // this.map.setView([this.jeu.latitude, this.jeu.longitude], this.scale);
              
                
                // leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                //   attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                //   maxZoom: 20
                // }).addTo(this.map);
                 

                this.subscription = this.geolocation.watchPosition({timeout: 30000, maximumAge: 0, enableHighAccuracy: true})
                .filter((p) => p.coords !== undefined)
                .subscribe((response)=>{
                  // alert("response.coords "+JSON.stringify(response))
                  that.now = Date.now();
                  // var LeafIcon = leaflet.Icon.extend({
                  //     options: {
                  //         /*shadowUrl: 'leaf-shadow.png',*/
                  //         iconSize:     [38, 95],
                  //         //shadowSize:   [50, 64],
                  //         iconAnchor:   [22, 94],
                  //         shadowAnchor: [4, 62],
                  //         popupAnchor:  [-3, -76]
                  //     }
                  // });
                  // var mylocationIcon = new LeafIcon({iconUrl: '../../assets/imgs/location_red.png'});
                  // var targetIcon = new LeafIcon({iconUrl: '../../assets/imgs/marker-icon.png'})
                  // let markerGroup = leaflet.featureGroup();
                  that.geolatitude = response.coords.latitude;
                  that.geolongitude= response.coords.longitude;
                  // if(response.coords && response.coords.longitude){
                  that.myPosition = [response.coords.longitude, response.coords.latitude];
                  // }
                  // alert("response.coords "+JSON.stringify(response.coords))
                  
                  // that.myposition = leaflet.circle([that.geolatitude, that.geolongitude]/*,{icon: mylocationIcon}*/, {radius: 2}).addTo(this.map);
                  // that.myposition = leaflet.circle([that.geolatitude, that.geolongitude], {
                  //     color: 'red',
                  //     fillColor: '#f03',
                  //     fillOpacity: 0.5,
                  //     radius: 5
                  // }).addTo(that.map);
                  // markerGroup.addLayer(that.myposition);
                  // that.map.addLayer(markerGroup);
                  if(Math.sqrt(Math.pow(Number(that.geolatitude)-Number(that.jeu.latitude),2)+Math.pow(Number(that.geolongitude)-Number(that.jeu.longitude),2))<0.002){
                      
                      let title = 'vous êtes arrivés';
                      that.translate.get([title, 'ok']).subscribe((value)=>{
                   
                            const prompt = this.alertCtrl.create({
                            message: '' ,
                            title: value[title],
                            buttons: [
                                {
                                  text: value['ok'] ,
                                  handler: data => {
                                    that.nextGame();
                                  }
                                }
                            ],
                            cssClass:"alert-geohunt"
                            });
                            prompt.present();
                      
                      })
                      that.subscription.unsubscribe();
                  }
                  // console.log(Math.sqrt(Math.pow(Number(this.geolatitude)-Number(this.jeu.latitude),2)+Math.pow(Number(this.geolongitude)-Number(this.jeu.longitude),2)));

                  // console.log(that.now);
                },(err)=>{
                  alert("Error "+ JSON.stringify(err));
                });

                // let markerGroup1 = leaflet.featureGroup();
                // let marker1 = leaflet.marker([this.jeu.latitude,this.jeu.longitude]/*,{icon: targetIcon}*/).addTo(this.map);
                // markerGroup1.addLayer(marker1);
                // this.myposition = leaflet.circle([this.geolatitude, this.geolongitude],{icon: mylocationIcon}, {radius: 2}).addTo(this.map);
                // markerGroup1.addLayer(this.myposition);
                // this.map.addLayer(markerGroup1);
                // setTimeout(() => {
                //   this.map.invalidateSize(); 
                // }, 1000);
    }
        
    //this.map.addLayer(markerGroup1);

    //let markerMobile = leaflet.marker([this.geolatitude,this.geolongitude]).addTo(this.map);
    //markerGroup.addLayer(circle);
    
    //markerGroup.addLayer(markerMobile);
    
   
    // this.map.locate({
    //   setView: true,
    //   maxZoom: 19
    // }).on('locationfound', (e) => {
    //   let markerGroup = leaflet.featureGroup();
    //   let marker: any = leaflet.marker([e.latitude, e.longitude]).on('click', () => {
    //     this.maplatitude = e.latitude;
    //     this.maplongitude = e.longitude;
    //     //let reponse= 'latitude: '+e.latitude+' longitude: '+e.longitude
    //     //alert(this.maplatitude+','+this.maplongitude);
    //   })
    //   markerGroup.addLayer(marker);
    //   this.map.addLayer(markerGroup);
    //   }).on('locationerror', (err) => {
    //     //alert(err.message);
    // })
 


  showBGgeoloc(){
    
  }

  skipGame(){

    let cancel = "ANNULER";
    let accept = "ACCEPTER";
    let warning = "Des pénalisations vous seront remis si vous passer directement au suivant";
    let penal_indice = "Temps pénalisé : ";
    this.translate.get([cancel, accept, warning]).subscribe(
      value => {
      let confirm = this.alertCtrl.create({
        title: value[warning]+' : <br>'+this.jeu.penalisation_passer+" s",
        buttons: [
        {
          text: value[cancel],
          handler: () => {

          }
        },
        {
          text: value[accept],
          handler: () => {
            this.etat = true; 
            this.nextGame();

          }
        }
        ],
        cssClass: 'alert-geohunt'
      });
      confirm.present(); 
    })
  }


  indiceJeu(){
    if(this.jeu.indices.length > 0){
      this.cpt.penalisation_indices(this.jeu,this.jeu.aventures_id,this.jeu.key);
    }
  }
  
  isArrived(){
    // alert((Math.sqrt(Math.pow(Number(this.geolatitude)-Number(this.jeu.latitude),2)+Math.pow(Number(this.geolongitude)-Number(this.jeu.longitude),2))));
    let title= "vous n'êtes pas encore arrivés";
    let message= 'vous êtes arrivés';
    let suivant = 'Suivant';
    this.translate.get([title,message,suivant]).subscribe((value)=>{
      if((Math.sqrt(Math.pow(Number(this.geolatitude)-Number(this.jeu.latitude),2)+Math.pow(Number(this.geolongitude)-Number(this.jeu.longitude),2)))<0.002){
        const prompt = this.alertCtrl.create({
        title:value[message],
        buttons: [
            {
              text: value[suivant] ,
              handler: data => {
                this.nextGame();
              }
            }
        ],
        cssClass:'alert-geohunt'
        });
        prompt.present();   
      }
      else{
        this.alert('',value[title],'Ok');
      }
    })
  }
  alert(message:string,title:string='',textButton:string,classAlert:string='alert-geohunt'){
    this.translate.get([message,textButton]).subscribe((value)=>{
        const prompt = this.alertCtrl.create({
        message: value[message] ,
        title:title,
        buttons: [
            {
              text: value[textButton] ,
              handler: data => {

              }
            }
        ],
        cssClass:classAlert
        });
        prompt.present();
    })
  }
  nextGame(){

    if(this.etat == true){
      this.cpt.endTimerJeu(this.jeu.type,this.jeu.sequence,this.jeu.key,this.jeu.aventures_id,this.jeu.penalisation_passer,true);
      this.etat = false;
    } else {
      this.cpt.endTimerJeu(this.jeu.type,this.jeu.sequence,this.jeu.key,this.jeu.aventures_id,this.jeu.penalisation_passer,false);
    }

    switch (this.jeuSuivant.type) {
     case "quizz":
        // code...
        this.navCtrl.push(QuizzPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "puzzle":
        // code...
        this.navCtrl.push(PuzzlesPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "geolocalisation":
        // code...
        this.navCtrl.push(GeolocalisationPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "qrcode":
        // code...
        this.navCtrl.push(QrcodePage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "photo":
        // code...
        this.navCtrl.push(PhotoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "info":
        // code...
        this.navCtrl.push(InfoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      default:
        // code...
        this.navCtrl.push(LeaderboardPage,{id_aventure:this.jeu.aventures_id,dernierJeu:this.jeu});
        break;
    }
    //this.navCtrl.push(PuzzlesPage);
    //this.cpt.endTimerJeu("geolocalisation",7);

  }

}
