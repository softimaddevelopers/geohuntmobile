import { Component } from '@angular/core';
import { FabContainer, Events} from 'ionic-angular';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { FaqPage } from '../faq/faq';
import  { GeolocalisationPage } from '../geolocalisation/geolocalisation';

import  { QrcodePage } from '../qrcode/qrcode';
import { LeaderboardPage} from '../leaderboard/leaderboard';
// import { PuzzleThreePage } from '../puzzle-three/puzzle-three';
//import { PaiementPage} from '../paiement/paiement';

//import  { QrcodePage } from '../qrcode/qrcode';

import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {



  tab1Root = HomePage;
 // tab1Root = QrcodePage;
  // tab1Root = PuzzleThreePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = GeolocalisationPage;
  tab5Root = FaqPage;
  lang = [{title : 'fr', flag: "fr"}, {title : 'en', flag: "gb"}]

  constructor(
  	public translate: TranslateService,
    public events: Events
  ) {
    let self = this;
    this.events.subscribe('language', (language) => {
      // alert(JSON.stringify(language));
      if(language){
        self.lang = []
        for(let i in language){
          if(language[i]){
            if(i == "fr"){
              self.lang.push({title : 'fr', flag: "fr"});
            }
            if(i == "en"){
              self.lang.push({title : 'en', flag: "gb"});
            }
          }
        }
      }
    });
  }

  changeLang(lang:string, fab:FabContainer){
  	this.translate.use(lang);
  	fab.close();

  }
}
