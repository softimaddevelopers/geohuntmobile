import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,AlertController } from 'ionic-angular';
import { Stripe} from '@ionic-native/stripe';
import { Card } from '../../models/card';
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the CardInfoStripePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-card-info-stripe',
  templateUrl: 'card-info-stripe.html',
})

/*export class Card {
 	number:string;
 	expMonth:any;
 	expYear:any;
 	cvc:string;
}*/
export class CardInfoStripePage {
  

  /*Card= new Card('5555555555554444', null, null, null);*/
  public transaction= false;
  public prix_aventure:string;
  public alertCtrl:AlertController;
  public testCardNumber=false;
  public testDate=false;
  public testCvc=false;
  public showError=false;
  public card :Card;
  public tempCard:string;
  public today:any;
  public date:string;
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public viewCtrl: ViewController,
  	public stripe:Stripe,
    public translate: TranslateService
  ) {
  	this.prix_aventure = this.navParams.get('prix_aventure');
    this.today = new Date();
  	this.card ={
  		number:'',
  		expMonth:null,
  		expYear:null,
  		cvc:''
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CardInfoStripePage');
  }
  quitStripe(){
    this.viewCtrl.dismiss({quit:true});
  }
  pay(){
	  	//verification des informations via  stripe;
	  		let that = this;
        this.transaction = true;
        this.card.number= this.tempCard;
        this.card.expMonth=this.date.substr(0,2);
        this.card.expYear = this.date.substr(3,2);
		  	this.stripe.setPublishableKey('pk_test_KuzcNWLeTxEhqWBDJuKsX5ID')
		  	this.stripe.validateCardNumber(this.card.number).then((valide)=>{
		  		//alert('validation Number')

		  		that.testCardNumber = true;
		  		that.stripe.validateCVC(that.card.cvc).then((valide)=>{
			  		that.testCvc = true;
			  		that.stripe.validateExpiryDate(that.card.expMonth,that.card.expYear).then((valide)=>{
				  		that.testDate = true;
              that.transaction= false;
				  		that.viewCtrl.dismiss(that.card);
				  	}).catch((err)=>{
				  		this.showError = true;
              that.transaction= false;
				  		//alert('error expiry date')


				  	});
			  	}).catch((err)=>{
			  		this.showError =true;
            that.transaction = false;
			  		//alert('error cvc')
			  	});
		  		
		  	}).catch((err)=>{
		  		this.showError =true;
          that.transaction=true
		  		//alert('error card');
		  	});
	}
  cardification(){
    this.card.number = this.setPerFour(this.card.number);
    this.tempCard = this.NoSpaceCard(this.card.number);
  }
  alert(message:string,textButton:string,classAlert:string='alert-geohunt'){
    this.translate.get([message,textButton]).subscribe((value)=>{
        const prompt = this.alertCtrl.create({
        message: value[message] ,
        buttons: [
            {
              text: value[textButton] ,
              handler: data => {

              }
            }
        ],
        //cssClass:classAlert
        });
        prompt.present();
    })
  }
  setPerFour(cardNumber:string){
    let arrayCard = cardNumber.split(" ");
    let strCard = '';
    let cardPerFour='';
    for(var i=0;i<arrayCard.length;i++){
      strCard+= arrayCard[i];
    }
    for(i= 0;i<Math.floor(strCard.length/4);i++){
      cardPerFour+= strCard.substr(i*4,4)+" ";
    }
    console.log(cardPerFour.length);
    return cardPerFour;
  }
  NoSpaceCard(card:string){
   let arrayCard = card.split(" ");
   let strCard= '';
   for(var i= 0 ;i<arrayCard.length;i++){
     strCard+=arrayCard[i]
   }
   console.log(strCard);
   return strCard;
  }
  datification(){
    this.date = this.setFormatDate(this.date);
    console.log('month:', this.date.substr(0,2));
    console.log('year:', this.date.substr(3,2));
  }
  setFormatDate(date:string){
    let arrayDate= date.split("/");
    let strDate ='';
    let datePerTwo=''
    for(var i=0;i<arrayDate.length;i++){
      strDate+=arrayDate[i];
    }
    datePerTwo = strDate.substr(0,2)+"/"+strDate.substr(2,2);
    console.log(datePerTwo);
    return datePerTwo;
  }
}
