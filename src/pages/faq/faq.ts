import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AventureProvider } from '../../providers/aventure/aventure';
import { TranslateService } from '@ngx-translate/core';
import { Lang } from "../../models/lang-model";

/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
	termes:Lang = new Lang();
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public adv : AventureProvider,
    public translate: TranslateService
    ) {

  	this.setTermes();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
  }

  setTermes(){
    let that = this;
    this.adv.get("/faq").snapshotChanges().subscribe((data) => {
      that.termes = new Lang().deserialize(data.payload.val());
 		});
  }

}
