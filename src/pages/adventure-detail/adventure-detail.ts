import { Component } from '@angular/core';
import { /*IonicPage,*/ NavController, NavParams, AlertController,Platform, ModalController,LoadingController, Events } from 'ionic-angular';
import { AventureProvider } from '../../providers/aventure/aventure';
import { Aventure } from "../../models/aventure-model";
import { QuizzPage } from '../quizz/quizz';
import { PuzzlesPage } from '../puzzles/puzzles';
import { PhotoPage} from '../photo/photo';
import { GeolocalisationPage } from '../geolocalisation/geolocalisation';
import { QrcodePage } from '../qrcode/qrcode';
import { LeaderboardPage } from '../leaderboard/leaderboard';
import { ComptageProvider } from '../../providers/comptage/comptage';
import  { PaiementProvider } from '../../providers/paiement/paiement';
import { FileTransfer,/* FileUploadOptions,*/ FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
//import { EmailComposerOriginal } from '@ionic-native/email-composer';
//import { EmailComposer } from '@ionic-native/email-composer';
import { PaiementPage } from '../paiement/paiement';
import { Http, Headers } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { Storage } from '@ionic/storage';
import { NgProgress } from 'ngx-progressbar';
//import { Diagnostic } from '@ionic-native/diagnostic';
import { TranslateService } from '@ngx-translate/core';
import { Lang } from "../../models/lang-model";
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';

import { Jeux } from '../../models/jeux-model';
import { InfoPage } from '../info/info';

/**
 * Generated class for the AdventureDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-adventure-detail',
  templateUrl: 'adventure-detail.html',
})
export class AdventureDetailPage {

  public urlMailServer:string ='https://geohuntserver.herokuapp.com/sendmail' ;
  //public urlMailServer:string ='https://servicegeohunt.firebaseapp.com/sendmail' ;
  public headers   : any    = new Headers({ 'Content-Type': 'application/json' });
  code_confirmation:string;
  email_random:string;

  termes:Lang = new Lang();

  key_aventure:any;
 
  pathData:string='/aventures';
  titre:string;
  description:string;
  temps_min:number;
  temps_max:number;
  niveau:string;
  idAventures:string;
  test:string = "balbala";
  avatar:Blob;
  imageSrc:any;
  aventure: Aventure = new Aventure();
  aventures: Aventure[];
  plus:boolean =false;
  afficherEmailForm:boolean=false;// afficher loader
  payed:boolean =false;
  date_paiement:any;
  jeux:any=[];
  fileTransfer: FileTransferObject = this.transfer.create();
  jeuxLocal:any=[];
  //loader:Loading;
  joueur_key;
  paiement_key; 


  //jeuxLocal:any=[];
  dernierSequence=1;
  password:string;
  progress:number=0;
  loading:any;
  end:Boolean = false;
  paie;
  joueur_id;
  date_deb_av:any;

  web:Boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public db: AngularFireDatabase,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController, 
    public adv : AventureProvider,
    public http:Http,
    private nativeStorage: NativeStorage,
    public loadingCtrl: LoadingController,
    private transfer: FileTransfer,
    public plt:Platform, 
    private file: File,

    //private diagnostic:Diagnostic,
    public paiement: PaiementProvider,
    public cpt : ComptageProvider,

    public ngProgress: NgProgress,
    //private diagnostic:Diagnostic,
    public translate:TranslateService,
    public events: Events
    //private emailComposer: EmailComposer

    ) {

    

  }

  ionViewDidLoad(){
    this.setTermes();

    this.joueur_key = this.navParams.get("joueur_key");
    this.paiement_key = this.navParams.get("paiement_key");
    this.key_aventure = this.navParams.get("key_aventure");

    this.aventure = new Aventure().deserialize(JSON.parse(this.navParams.get("aventure")));

    this.code_confirmation = this.randomAlphaNum(8);

    this.email_random = this.randomAlphaNum(4+Math.floor(Math.random()*6))+'@'+this.randomAlphaNum(4+Math.floor(Math.random()*6))+'.com';

    this.nativeStorage.getItem('dernierSequence'+this.key_aventure).then((data)=>{
      this.dernierSequence = data.sequence;

    },(err)=>{
      this.dernierSequence = 1;
    })
    this.nativeStorage.getItem('paiement'+this.key_aventure).then(
      (data:any)=>{
        //alert(JSON.stringify(data));
        if(this.key_aventure == data.aventureId){
          this.payed = data.payed;
        }
        this.nativeStorage.getItem('confirmation'+this.key_aventure).then((res)=>{
        },(err)=>{
          if(Number(this.aventure.prix) > 0){
            this.enregistrerMail();
          }
          
        })
        this.afficherEmailForm = true;
      },
      (err)=>{
        // tsy manao inin ref tsis donnee
        //alert(err)
    })
    console.log(this.key_aventure);
    let self = this;
     this.adv.get("/aventures/"+this.key_aventure).snapshotChanges().subscribe((data) =>{
      self.aventure = new Aventure().deserialize({ key: data.key, ...data.payload.val() });
      this.events.publish('language', this.aventure.language);
      console.log(self.aventure);
      // if(Number(self.aventure.prix) == 0){
      //   self.offline()
      // }
    })
    
     //chargement du jeuxLocal
     this.key_aventure = this.navParams.get("key_aventure");
     
     // get joueur_id
    this.nativeStorage.getItem("paiement"+this.key_aventure).then((paie)=>{
      this.joueur_id = paie.joueur_id;
      // attribuer date debut aventure
      this.nativeStorage.getItem("dateDeb"+this.key_aventure).then((dateDeb) =>{
        this.date_deb_av = dateDeb;
        
      });
    });

    if(!window.cordova){
      this.web = true;
    }
    this.nativeStorage.getItem('leaderboard'+this.key_aventure).then(function(l){
      this.end = l;
    });

    this.nativeStorage.getItem('joueurJeux').then((data:any)=>{
                                                                   
                                                                 }).catch((err) =>{
                                                                   // alert("JoueurJeux not found "+JSON.stringify(err));
                                                                 });
    this.nativeStorage.getItem('loaded'+this.key_aventure).then((data)=>{
        this.jeuxLocal= data.jeux;
      },(err)=>{ 
        // this.loadingOnlineData();
        if(!window.cordova){
          // alert('web');
          this.loadingOnlineData();
        
        }
        
     })
    
  }

  loadingOnlineData(){
    let self = this;
    const loader = this.loadingCtrl.create({
      content: "Loading...",
    });
    loader.present();
    console.log("getting games");
    this.adv.get('/jeux').snapshotChanges().subscribe((data)=>{
                    /* self.jeux= data.payload.val();*/
                    loader.dismiss();
                     for (var key in data.payload.val()) {
                       // code...
                       if(self.key_aventure == data.payload.val()[key].aventures_id){
                         self.jeuxLocal.push({key:key,... data.payload.val()[key]});
                       }
                     }
                     console.log("jeux online %o",self.jeux)
                    // alert('get aventure tsik izao');
                     this.nativeStorage.getItem('loaded'+this.key_aventure).then((data)=>{
                       self.jeux= data.jeux;
                       //alert('le jeu local est deja charger')
                     },(err)=>{
                       //alert(err)
                       // this.offline(self.jeux)// charge le this.jeuxLocal, mila connexion eto
                      
        })
      });
  }

  setProgress(value){
    this.progress = value;
  }

  alert(message:string,textButton:string,classAlert:string=''){
    this.translate.get([message,textButton]).subscribe((value)=>{
        const prompt = this.alertCtrl.create({
        message: value[message] ,
        buttons: [
            {
              text: value[textButton] ,
              handler: data => {

              }
            }
        ],
        //cssClass:classAlert
        });
        prompt.present();
    })
  }
  transferOver(){
    let message = 'le transfert est terminé.'
    this.translate.get([message]).subscribe((value)=>{
      let transferOver= this.alertCtrl.create({
        message:value[message],
        buttons:[{
          text:'ok',
          handler: data=>{

          }
        }]
      });
      transferOver.present();
    })
    
  }

  offline(){
      let download = "Download";
      let self = this;
      this.translate.get([download]).subscribe((value)=>{

            const loader = this.loadingCtrl.create({
                content: value[download]
            });
            loader.present();
            
            let loadingImage = false;

            let ref = this.db.database.ref('jeux').once("value", (data) => {
           

              let jeux:Jeux[] = [];
              data.forEach( (dataChild) => {
                let jeu = new Jeux().deserialize(dataChild.val());
                jeu.key = dataChild.key;
                jeux.push(jeu);
              })
             
                          
                            let promises = [];
                            jeux.forEach( (jeu, index, array) => {



                              if(self.key_aventure == jeu.aventures_id){
                                
                                   
                               if(jeu.image_url != undefined ){
                                  if(jeu.image_url.includes("http")){
                                       let extension = self.fileExtension(jeu.image_url);
                                      

                              
                                      let imageLocal = self.file.dataDirectory+ jeu.aventures_id+ "/" + jeu.key + '/image.' + extension;
                                  
                                      promises.push(self.fileTransfer.download(jeu.image_url, imageLocal).then(() =>{
                                        
                                      }));


                                          




                                          
                                          jeu.image_url = self.file.dataDirectory+ jeu.aventures_id+ "/" + jeu.key + '/image.' + extension;
                                         
                                     
                                       
                                     
                                  }
                                     
                              }

                              self.jeux.push(jeu);
       
                               
                            }
                         
                           })


                          Promise.all(promises).then(() =>{
                   
                            loadingImage = false;
                            loader.dismiss();
                            this.nativeStorage.getItem('loaded'+this.key_aventure).then((data2)=>{
                                                       self.jeux= data2.jeux;
                                                       self.jeuxLocal = data2.jeux;
                                                       if(!loadingImage){
                                                         loader.dismissAll();
                                                       }
                                                        if(Number(self.aventure.prix) == 0){
                                                          let joueur_key = this.db.list("/joueur").push({firstname: "anonymous", lastname:"anonymous" }).key;
                                                          this.joueur_key = joueur_key;
                                                          
                                                          this.nativeStorage.setItem(
                                                            'paiement'+this.key_aventure,{payed:true,aventureId:this.key_aventure,key_paiment:"none", joueur_id:joueur_key}).then(() =>{
                                                            let joueurJeux ={};
                                                            joueurJeux[this.key_aventure] = {};
                                                            joueurJeux[this.key_aventure][joueur_key] = {};
                                                            joueurJeux[this.key_aventure][joueur_key].date_deb_aventure =  Date.now();

                                                            this.nativeStorage.getItem("joueurJeux").then((dataJoueurJeux) =>{
                                                                joueurJeux = dataJoueurJeux;
                                                                joueurJeux[this.key_aventure] = {};
                                                                joueurJeux[this.key_aventure][joueur_key] = {};
                                                                joueurJeux[this.key_aventure][joueur_key].date_deb_aventure =  Date.now();
                                                            }, (err) =>{
                                                                let joueurJeux ={};
                                                                joueurJeux[this.key_aventure] = {};
                                                                joueurJeux[this.key_aventure][joueur_key] = {};
                                                                joueurJeux[this.key_aventure][joueur_key].date_deb_aventure =  Date.now();
                                                                this.nativeStorage.setItem('joueurJeux',joueurJeux).then((data)=>{
                                                                   this.nativeStorage.setItem("password"+this.key_aventure, true).then(() =>{

                                                                     self.start();

                                                                   })
                                                                   

                                                                },(err)=>{
                                                                  alert("Error "+JSON.stringify(err));
                                                                });
                                                            })
                                                                
                                                            
                                                          }).catch((err) =>{
                                                            alert("Cannot download : storage error");
                                                          })
   
                                                          
                                                       }
                                                       //alert('le jeu local est deja charger')
                                                     },(err)=>{
                                                       this.nativeStorage.setItem('loaded'+this.key_aventure,{jeux:self.jeux}).then((data3)=>{
                                                             self.jeux= data3.jeux;
                                                             self.jeuxLocal = data3.jeux;
                                                             if(!loadingImage){
                                                               loader.dismissAll();
                                                             }
                                                             if(Number(self.aventure.prix) == 0){
                                                                let joueur_key = this.db.list("/joueur").push({firstname: "anonymous", lastname:"anonymous" }).key;
                                                                this.joueur_key = joueur_key;
                                                                
                                                                this.nativeStorage.setItem(
                                                                  'paiement'+this.key_aventure,{payed:true,aventureId:this.key_aventure,key_paiment:"none", joueur_id:joueur_key}).then(() =>{
                                                                  let joueurJeux ={};
                                                                  joueurJeux[this.key_aventure] = {};
                                                                  joueurJeux[this.key_aventure][joueur_key] = {};
                                                                  joueurJeux[this.key_aventure][joueur_key].date_deb_aventure =  Date.now();

                                                                  this.nativeStorage.getItem("joueurJeux").then((dataJoueurJeux) =>{
                                                                      joueurJeux = dataJoueurJeux;
                                                                      joueurJeux[this.key_aventure] = {};
                                                                      joueurJeux[this.key_aventure][joueur_key] = {};
                                                                      joueurJeux[this.key_aventure][joueur_key].date_deb_aventure =  Date.now();
                                                                  }, (err) =>{
                                                                      let joueurJeux ={};
                                                                      joueurJeux[this.key_aventure] = {};
                                                                      joueurJeux[this.key_aventure][joueur_key] = {};
                                                                      joueurJeux[this.key_aventure][joueur_key].date_deb_aventure =  Date.now();
                                                                      this.nativeStorage.setItem('joueurJeux',joueurJeux).then((data)=>{
                                                                         this.nativeStorage.setItem("password"+this.key_aventure, true).then(() =>{

                                                                           self.start();

                                                                         })
                                                                         

                                                                      },(err)=>{
                                                                        alert("Error "+JSON.stringify(err));
                                                                      });
                                                                  })
                                                                      
                                                                  
                                                                }).catch((err) =>{
                                                                  alert("Cannot download : storage error");
                                                                })
                                                             }
                                                        })
                                                      
                                                  });
                          }).catch((err) =>{
                            alert("Download Error  "+JSON.stringify(err));
                            loader.dismissAll();
                          })
                    
                   
                                                  
              });
      })
            
      
      
  }

                    
  


  randomAlphaNum(length=4){
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let retour='';
    for (var i=0;i<length;i++){
      retour+=possible[Math.floor(Math.random()*possible.length)];
    }
    return retour;
  }
  
  enregistrerMail() {
    let message = "Veuillez entrer votre adresse email pour recevoir un code d’accès";
    let promptTitle = 'Email';
    let envoyer='Envoyer';
    let erreurMailformat= 'Verifier le format de votre adresse.';
    let erreurSendingMail='Votre mail n\'a pas été envoyé.';
    let emailSucces='Un mail vous a été envoyé.';
    let regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let self = this;
    //let regexMail= /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    let popupContent = 'Envoi mail';
    this.translate.get([message,envoyer,promptTitle,popupContent,erreurMailformat,erreurSendingMail,emailSucces]).subscribe((value)=>{
        const prompt = this.alertCtrl.create({
        title: value[promptTitle] ,
        message: value[message] ,
        inputs: [
          {
            name: 'email',
            placeholder: 'Email',
            type:'email'
          },
        ],
        buttons: [
          {
            text: value[envoyer] ,
            handler: data => {
                if(!regexMail.test(data.email)){
                  this.alert(value[erreurMailformat],'ok');
                }
                else{
                  let email = {
                    to: data.email,
                    subjectcode:this.code_confirmation,
                    randomEmail:this.email_random
                  };
                  let loadingPopup = this.loadingCtrl.create({
                    content: value[popupContent]
                  });
                  // Show the popup
                  loadingPopup.present();
                  this.http.post(this.urlMailServer,email,this.headers).subscribe((resp:any)=>{

                    if(JSON.parse(resp._body).succes == true){
                       this.alert(value[emailSucces],'ok');

                       this.nativeStorage.setItem('joueur',{email:data.email});

                       this.nativeStorage.getItem('paiement'+this.key_aventure).then((data)=>{
                         // alert("ok update paiement");
                         this.adv.update('/paiement',data.key_paiment,{password:JSON.parse(resp._body).subjectcode}).then(()=>{
                           // alert("update success");
                         });
                       });

                       this.nativeStorage.setItem('confirmation'+this.key_aventure,{code:JSON.parse(resp._body).subjectcode, email:JSON.parse(resp._body).randomEmail});
                       
                       // this.adv.get('/jeux').snapshotChanges().subscribe((data)=>{
                        /* self.jeux= data.payload.val();*/
                         // for (var key in data.payload.val()) {
                         //   // code...
                         //   if(self.key_aventure == data.payload.val()[key].aventures_id){
                         //     self.jeux.push(data.payload.val()[key]);
                         //   }
                         // }
                        //alert('get aventure tsik izao');
                         this.nativeStorage.getItem('loaded'+this.key_aventure).then((data)=>{
                           loadingPopup.dismiss();
                         },(err)=>{
                           loadingPopup.dismiss();
                           self.offline()// charge le this.jeuxLocal, mila connexion eto
                         })
                       // })
                    }
                    else{
                      this.alert(value[erreurSendingMail],'ok')
                      loadingPopup.dismiss();
                      //alert(JSON.stringify(resp));
                    }
                    
                    },(err)=>{
                        // alert(JSON.stringify(err));
                        //alert("Sending mail error!");
                        this.alert(value[erreurSendingMail],'ok')
                        loadingPopup.dismiss();
                        })
                      }
                    }
              }  
          ]
        });
        prompt.present();
    })
  }

  verificationCode() {
    //alert(JSON.stringify(this.jeuxLocal));
    //alert(this.dernierSequence);
    this.nativeStorage.getItem('password'+this.key_aventure).then((data)=>{
        if(data){
            this.checkStorage();
        }
    }, (err) => {

            let promptTitle = 'Mot de passe';
            let messagePrompt = "Veuillez consulter votre boîte mail et saisir le code d’accès";
            let placeHolderPrompt = 'Saisir le code d’accès';
            let envoyer= "Valider";
              
              this.translate.get([promptTitle,messagePrompt,placeHolderPrompt,envoyer]).subscribe( (value)=>{
                  const prompt = this.alertCtrl.create({
                  title: value[promptTitle],
                  message: value[messagePrompt],
                  inputs: [
                     {
                      name: 'code',
                      placeholder: value[placeHolderPrompt],
                      type:'text'
                    },
                  ],
                  buttons: [
                    {
                      text: value[envoyer],
                      handler: data => {
                        this.nativeStorage.getItem('confirmation'+this.key_aventure).then((resp)=>{
                            if( data.code == resp.code /*&& data.email == resp.email*/ ){
                                this.nativeStorage.setItem("password"+this.key_aventure, true);
                                this.checkStorage();
                            }
                        },(err)=>{
                          this.alert(JSON.stringify(err),'ok');
                        })
                      }
                    }
                  ]
                });
                prompt.present();
              }); 
            
    }); 
  }

  start(){
    let jeu:any;
    for(var i=0;i<this.jeuxLocal.length;i++){
      if(this.jeuxLocal[i].sequence == this.dernierSequence){
        jeu = this.jeuxLocal[i];
        break;
      }
      else jeu={};
    }
                              //alert(JSON.stringify(jeu));
                              // alert("calling next page esle");
    this.nextPage(jeu);
  }

  demos(){
    this.nextPageTest(this.jeux[0]);
  }

 termesConfirm(prix:string) {

      let cancel = "ANNULER";
      let accept = "ACCEPTER";
      if(Number(prix)==0){
        this.offline()
      } 
      else{
        this.translate.get([cancel, accept]).subscribe(
            value => {

                let alert = this.alertCtrl.create({
                  // title:"TERMES",
                  message: this.termes[this.translate.currentLang],
                  buttons: [
                    {
                      text: value[cancel],
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    {
                      text: value[accept],
                      handler: () => {
                        console.log('Buy clicked');
                        this.paiment(prix);
                      }
                    }
                  ]
                });
                alert.present();
          })
      }   
  }

  paiment(prix:string){
    this.navCtrl.push(PaiementPage,{prix_aventure:prix, aventure:this.aventure,});
  }
  onKeydown(event){
  	console.log("keypress");
  	console.log(event)
  	if(event.key == "Enter"){
  		this.navCtrl.push(QuizzPage);
  	}
  }




  checkStorage(){

    //alert("au debut");
    //alert(this.joueur_id);
    //alert(this.key_aventure);
    

    for(var i=0;i<this.jeuxLocal.length;i++){

      // get indice jeu en cours si existe
      

    }
    let self = this;
    var i = 0;
    function next(){
      
      self.nativeStorage.getItem('indicesPenalisations'+self.key_aventure+self.jeuxLocal[i].key).then((data)=>{
        // alert( "JEUX "+JSON.stringify(self.jeuxLocal[i].key));
       
        // alert(i+ ' '+self.jeuxLocal.length);
        self.jeuxLocal[i].indices = data.indices;   
        // alert( "indices length"+JSON.stringify(self.jeuxLocal[i].indices.length));
        if(i == self.jeuxLocal.length -1){

          self.start();
        }else{
          i =i +1; 
          next();
        }

      },(err)=>{
        // alert("indices not exist in storage");
        // attribuer indices pour le jeu local si non existant
        self.nativeStorage.setItem('indicesPenalisations'+self.key_aventure+self.jeuxLocal[i].key,
          {indices: self.jeuxLocal[i].indices}).then((success)=>{
            console.log("indice sauvé")
            if(i == self.jeuxLocal.length -1){
              self.start();
            }else{
              i =i +1; 
              next();
            }
          },(err)=>{ 
            console.log('indice non sauvé'+err);

        });  

      });     
        
    }

    next();

    // this.cpt.getAllJeux(this.jeuxLocal);

    
    //this.navCtrl.push(QuizzPage);
  }

  nextPage(jeuSuivant){
    this.cpt.date_debut(this.key_aventure,this.joueur_id, this.date_deb_av,Date.now());
    
    switch (jeuSuivant.type) {
      case "quizz":
        // code...
        //alert('quizz');
        this.navCtrl.push(QuizzPage,{jeux:this.jeuxLocal,ceJeu:jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "puzzle":
        // code...
        //alert('puzzle');
        this.navCtrl.push(PuzzlesPage,{jeux:this.jeuxLocal,ceJeu:jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "geolocalisation":
        // code...
        //alert('geolocalisation');
        this.navCtrl.push(GeolocalisationPage,{jeux:this.jeuxLocal,ceJeu:jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "qrcode":
        // code...
        //alert('qrcode');
        this.navCtrl.push(QrcodePage,{jeux:this.jeuxLocal,ceJeu:jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "photo":
        // code...
        //alert('photo');
        this.navCtrl.push(PhotoPage,{jeux:this.jeuxLocal,ceJeu:jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "info":
        // code...
        this.navCtrl.push(InfoPage,{jeux:this.jeuxLocal,ceJeu:jeuSuivant,joueur_key:this.joueur_key});
        break;
      default:
        // code...
        //alert('quizz');
        //console.log(this.jeux)
        this.navCtrl.push(LeaderboardPage,{id_aventure:this.key_aventure,dernierJeu:jeuSuivant});        
        break;
    }
  }

  nextPageTest(jeuSuivant:any={}){
    //alert('le code passe sur jeu suivant')
    this.cpt.date_debut(this.key_aventure,this.joueur_id, this.date_deb_av,Date.now());
    switch (jeuSuivant.type) {
      case "quizz":
        // code...
        //alert('quizz');
        this.navCtrl.push(QuizzPage,{jeux:this.jeux,ceJeu:jeuSuivant});
        break;
      case "puzzle":
        // code...
        //alert('puzzle');
        this.navCtrl.push(PuzzlesPage,{jeux:this.jeux,ceJeu:jeuSuivant});
        break;
      case "geolocalisation":
        // code...
        //alert('geolocalisation');
        this.navCtrl.push(GeolocalisationPage,{jeux:this.jeux,ceJeu:jeuSuivant});
        break;
      case "qrcode":
        // code...
        //alert('qrcode');
        this.navCtrl.push(QrcodePage,{jeux:this.jeux,ceJeu:jeuSuivant});
        break;
      case "photo":
        // code...
        //alert('photo');
        this.navCtrl.push(PhotoPage,{jeux:this.jeux,ceJeu:jeuSuivant});
        break;

      default:
        // code...
        //alert('quizz');
        console.log(this.jeux)

        this.navCtrl.push(GeolocalisationPage,{jeux:this.jeuxLocal,ceJeu:this.jeuxLocal[4],joueur_key:this.joueur_key});        

        break;
    }
    //this.navCtrl.push(QuizzPage);
  }

  setTermes(){

    let that = this;
    this.adv.get("/termes").snapshotChanges().subscribe((data) => {
      that.termes = new Lang().deserialize(data.payload.val());
   
    });

  }

  testJeux(){

              console.log("next page test %o", this.jeux);
              let jeu:any;

              for(var i=0;i<this.jeux.length;i++){
                console.log(this.jeux[i].sequence);
                if(this.jeux[i].sequence == 1){
                  jeu = this.jeux[i];
                  this.nextPageTest(jeu);
                  break;
                }
              }

  }


  leaderboard(){

        this.navCtrl.push(LeaderboardPage, {result:true, id_aventure:this.key_aventure});
      
  }

  fileExtension( url ) {
    return url.split('.').pop().split(/\#|\?/)[0];
  }


}
