import { Component } from '@angular/core';
import { /*IonicPage,*/ NavController, NavParams, Events, AlertController, Platform, App } from 'ionic-angular';
import { QRScanner,  QRScannerStatus } from '@ionic-native/qr-scanner';
import { ComptageProvider } from '../../providers/comptage/comptage';	
import { Jeux } from '../../models/jeux-model';
import { QrcodePage } from '../qrcode/qrcode';
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the ScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 //const ionApp = <HTMLElement>document.getElementsByTagName('ion-app')[0];
// const ionPage = <HTMLElement>document.getElementsByTagName('page-scan')[0];

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {

	jeu:Jeux = new Jeux();
	qrcode:QrcodePage;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public qrScanner: QRScanner,
  	public events: Events,
  	public cpt : ComptageProvider,
  	public alertCtrl:AlertController,
  	public platform: Platform,
  	public app:App,
  	public translate:TranslateService
  	) {
  		this.jeu= new Jeux().deserialize(this.navParams.get("jeu"));
  		this.qrcode = this.navParams.get("qrcode");
  		console.log(this.jeu);
  		//let self = this;
  		
 
  }

  ionViewDidLoad() {
    
  }

  	ionViewWillEnter(){
  		this.scan();
	}

	scan(){
		let self = this;
  		// const errAlert = self.alertCtrl.create({
				// 	      title: "<i class='fas fa-check-circle fa-2x'></i> <br> QR CODE TROUVE.",
				// 	      // subTitle: 'Bravo!.',
				// 	      buttons: [
				// 	        {
				// 	          text: 'SUIVANT',
				// 	          handler:() => {
				// 	          	console.log('here');
				// 	            // self.qrcode.nextGame();
				// 	          }
				// 	        }
				// 	      ],
				// 	      cssClass: 'alert-geohunt'
				// 	    });
				// 	    errAlert.present();

	  	this.qrScanner.prepare()
		  .then((status: QRScannerStatus) => {
		     if (status.authorized) {

		       // start scanning
		       let scanSub = this.qrScanner.scan().subscribe((text: string) => {
		         console.log('Scanned something', text);
		         
		         if(text == self.jeu.texte_qrcode){
					let text = "QR CODE TROUVE";
					let button = "SUIVANT"
		         	this.translate.get([text, button]).subscribe((value)=>{
		         			const successAlert = self.alertCtrl.create({
							      title: "<i class='fas fa-check-circle fa-2x'></i> <br>"+value[text]+".",
							      
							      buttons: [
							        {
							          text: value[button],
							          handler:() => {
							          	successAlert.dismiss();
							            self.qrcode.nextGame();
							          }
							        }
							      ],
							      cssClass: 'alert-geohunt'
							    });
							    successAlert.present();
		         	})
				         	
		         		
		         }else{
		         	let text = "Ce n'est pas le bon QR CODE";
					let button = "SUIVANT"
		         	this.translate.get([text, button]).subscribe((value)=>{
		         			const errAlert = self.alertCtrl.create({
						      title: "<i class='fas fa-exclamation-circle fa-2x'></i> <br>"+value[text]+"!",
								// subTitle: 'Essayez à nouveau.',
						      buttons: ['OK'],
						      cssClass: 'alert-geohunt'
						    });
						    errAlert.present();
		         	})
					      
		         }
		         self.qrScanner.hide(); // hide camera preview
		         scanSub.unsubscribe(); // stop scanning
		         self.scan();
		         
		       });
		       self.qrScanner.resumePreview();
		       self.qrScanner.show();

				
				// (document.getElementsByClassName('ion-page')[0] as HTMLElement).style.background = 'none transparent !important';
		     } else if (status.denied) {
		     	alert('Permission denied!');
		       // camera permission was permanently denied
		       // you must use QRScanner.openSettings() method to guide the user to the settings page
		       // then they can grant the permission from there
		     } else {
		       // permission was denied, but not permanently. You can ask for permission again at a later time.
		     }
		  })
		  .catch((e: any) => {
		  		alert('Error :'+e);
		  });
	}

	ionViewWillLeave(){
		this.qrScanner.destroy();
	}

}
