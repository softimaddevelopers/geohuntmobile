import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GeolocalisationPage} from "../geolocalisation/geolocalisation";
//import { QuizzPage } from '../quizz/quizz';
import { ComptageProvider } from '../../providers/comptage/comptage';
/**
 * Generated class for the ResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {

  temps_actuel:number;
  display_time:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public cpt : ComptageProvider) {
    this.temps_actuel = navParams.get('time');
    this.compter();
    //this.display_time = this.cpt.getHeure_min_sec(this.temps_actuel);
  }

  compter(){
    setInterval(function(){
      this.temps_actuel++;
      this.display_time = this.cpt.getHeure_min_sec(this.temps_actuel);

    }.bind(this),1000);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultPage');
  }

  nextPage(){
    this.navCtrl.push(GeolocalisationPage,{'time':this.temps_actuel});
  }

}
