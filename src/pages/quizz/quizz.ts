import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams , Platform, App, AlertController } from 'ionic-angular';

import { GeolocalisationPage} from "../geolocalisation/geolocalisation";
import { PuzzlesPage } from '../puzzles/puzzles';
import { PhotoPage} from '../photo/photo';
import { QrcodePage } from '../qrcode/qrcode';
import { LeaderboardPage} from '../leaderboard/leaderboard';
// import { NativeStorage } from '@ionic-native/native-storage';
import { ComptageProvider } from '../../providers/comptage/comptage';
import { Jeux } from '../../models/jeux-model';
import { TranslateService } from '@ngx-translate/core';

import * as stringSimilarity from "string-similarity";
import { AngularFireDatabase } from 'angularfire2/database';
import { NativeStorage } from '@ionic-native/native-storage';
import { File } from '@ionic-native/file';
import { InfoPage } from '../info/info';

/**
 * Generated class for the QuizzPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-quizz',
  templateUrl: 'quizz.html',
})
export class QuizzPage {
  jeu:Jeux = new Jeux();
  jeux:Jeux[];
  jeuSuivant:Jeux;
  reponse:string;


  show_indice:boolean = false;

  etat:any = false;
  joueur_key:any;
  listes_indices:any;
  nombre_indices:number;
  numero:number;
  nb_indices;
  constructor( 
    public db: AngularFireDatabase, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public cpt : ComptageProvider,
    private nativeStorage: NativeStorage,

    public platform: Platform,
    public app:App,
    public translate: TranslateService,
    public alertCtrl:AlertController,
    private file:File
    ) {
    
   

    
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizzPage');
    //this.joueur_key = this.navParams.get("joueur_key");
    this.jeu= new Jeux().deserialize(this.navParams.get("ceJeu"));
    this.jeux= this.navParams.get("jeux").map((a:Jeux) => new Jeux().deserialize(a));
    this.nativeStorage.setItem('dernierSequence'+this.jeu.aventures_id,{sequence:this.jeu.sequence});
    // alert('Jeu indices: '+JSON.stringify(this.jeu.indices));
    // alert('Jeu indices length: '+JSON.stringify(this.jeu.indices.length));
    console.log('Jeux: %o',this.jeux)
    console.log('JeuxSuivant: %o',this.jeuSuivant);

    if(this.jeu.image_url){
      let img = this.jeu.image_url;
      
      let correctPath = img.substr(0, img.lastIndexOf('/') + 1); 
      let currentName = img.substring(img.lastIndexOf('/')+1);
      this.file.readAsDataURL(correctPath, currentName).then( dataurl =>{
                this.jeu.image_url = dataurl;
              }).catch(function(err){
                alert('Error '+JSON.stringify(err));
      })
    }

    this.jeux = this.jeux.sort(function(a:Jeux, b:Jeux){return Number(b.sequence) -Number(a.sequence)});
    let curSequence:Number =  0
    for(var i=0;i<this.jeux.length;i++){
        curSequence = this.jeux[i].sequence;
  
        if(curSequence <= Number(this.jeu.sequence)) {
          this.jeuSuivant = (i!=0) ? this.jeux[i-1] : new Jeux();
          break;
        }else {
          this.jeuSuivant = new Jeux();
        }
    }

    this.nombre_indices = this.jeu.indices.length;

    this.nativeStorage.getItem("paiement"+this.jeu.aventures_id).then((paie)=>{
      this.joueur_key = paie.joueur_id;
    });   

    //console.log('Jeu: %o',this.jeu)
    //console.log('Jeux: %o',this.jeux)
    //console.log('JeuxSuivant: %o',this.jeuSuivant);
  
  }

  /*ngOnInit(){
    this.cpt.display_time;
    console.log(this.cpt.display_time);
  }*/

  onKeydown(event){
  

  	// if(event.key == "Enter"){
  	// 	this.navCtrl.push(GeolocalisationPage);
  	// }
  }

  validate(){
      if(this.reponse){
        let val = stringSimilarity.compareTwoStrings(this.jeu.reponse[this.translate.currentLang].toLowerCase(),this.reponse.toLowerCase());
          
        if(val >= 0.8){
         let ok = "BONNE REPONSE";
          let next = "Suivant";
           this.translate.get([ok, next]).subscribe((value)=>{
                 const successAlert = this.alertCtrl.create({
                  title:   "<b>"+this.jeu.reponse[this.translate.currentLang].toUpperCase()+"<b/> <i class='fas fa-check-circle' style='color:green'></i> <br>"+value[ok]+"".toUpperCase(),
                  
                  buttons: [
                    {
                      text: value[next],
                      handler:() => {
                        this.nextGame();
                     
                      }
                    }
                  ],
                  cssClass: 'alert-geohunt'
                });
                successAlert.present();
           })
                
        }else{

          let ok = "MAUVAISE REPONSE";
          let next = "Suivant";
           this.translate.get([ok, next]).subscribe((value)=>{
                 const successAlert = this.alertCtrl.create({
                  title:   "<i class='fas fa-exclamation-circle' style='color:red'></i> <br>"+value[ok]+"".toUpperCase(),
                  
                  buttons: [
                    {
                      text: "OK", 
                    }
                  ],
                  cssClass: 'alert-geohunt'
                });
                successAlert.present();
           })

        }
      }
  }

  // indiceJeu(){

  //   let indice_moin = this.nombre_indices - 1;
  //     alert(this.cpt.numero_suivant);
  //     if(this.cpt.numero_suivant !== undefined && this.cpt.numero_suivant <= this.nb_indices -1){

  //       this.cpt.penalisation_indices(this.listes_indices,this.cpt.numero_suivant,this.jeu.aventures_id,this.joueur_key,this.jeu.key, this.nb_indices -1);
       
  //     } else {
  //       this.cpt.penalisation_indices(this.listes_indices,this.numero,this.jeu.aventures_id,this.joueur_key,this.jeu.key, this.nb_indices -1);  
  //     }
  //     this.nombre_indices = indice_moin < 0 ? 0 : indice_moin;
  // }

  indiceJeu(){
    if(this.jeu.indices.length > 0){
      this.cpt.penalisation_indices(this.jeu,this.jeu.aventures_id,this.jeu.key);
    }
  }

  skipGame(){

    let cancel = "ANNULER";
    let accept = "ACCEPTER";
    let warning = "Des pénalisations vous seront remis si vous passer directement au suivant";
    let penal_indice = "Temps pénalisé : ";
    this.translate.get([cancel, accept, warning, penal_indice]).subscribe(
      value => {
      let confirm = this.alertCtrl.create({
        title: value[warning]+' : <br>'+this.jeu.penalisation_passer+" s",
        buttons: [
        {
          text: value[cancel],
          handler: () => {

          }
        },
        {
          text: value[accept],
          handler: () => {
            this.etat = true; 
            this.nextGame();

          }
        }
        ],
        cssClass: 'alert-geohunt'
      });
      confirm.present(); 
    })
  }

  nextGame(){

    this.nativeStorage.getItem('joueurJeux').then((data:any)=>{
      if(this.etat == true){
        this.cpt.endTimerJeu(this.jeu.type,this.jeu.sequence,this.jeu.key,this.jeu.aventures_id,this.jeu.penalisation_passer,true);
        this.etat = false;
      } else {
        this.cpt.endTimerJeu(this.jeu.type,this.jeu.sequence,this.jeu.key,this.jeu.aventures_id,this.jeu.penalisation_passer,false);
      }
    }); 
    switch (this.jeuSuivant.type) {
     case "quizz":
        // code...
        console.log('quizz ihany');
        this.navCtrl.push(QuizzPage,{jeux:this.jeux,ceJeu:this.jeuSuivant, joueur_key:this.joueur_key});
        break;
      case "puzzle":
        // code...
        this.navCtrl.push(PuzzlesPage,{jeux:this.jeux,ceJeu:this.jeuSuivant, joueur_key:this.joueur_key});
        break;
      case "geolocalisation":
        // code...
        this.navCtrl.push(GeolocalisationPage,{jeux:this.jeux,ceJeu:this.jeuSuivant, joueur_key:this.joueur_key});
        break;
      case "qrcode":
        // code...
        this.navCtrl.push(QrcodePage,{jeux:this.jeux,ceJeu:this.jeuSuivant, joueur_key:this.joueur_key});
        break;
      case "photo":
        // code...
        this.navCtrl.push(PhotoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant, joueur_key:this.joueur_key});
        break;
      case "info":
        // code...
        this.navCtrl.push(InfoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      default:
        // code..
        this.navCtrl.push(LeaderboardPage,{id_aventure:this.jeu.aventures_id,dernierJeu:this.jeu})
        break;
    }
  	//this.navCtrl.push(GeolocalisationPage);
    // this.cpt.endTimerJeu("quiz",1);
  	// this.navCtrl.push(GeolocalisationPage);
    
  }
}
