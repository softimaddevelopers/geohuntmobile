import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';

import { LeaderboardPage } from "../leaderboard/leaderboard";
import { ScanPage } from "../scan/scan";
import { ComptageProvider } from '../../providers/comptage/comptage';
import { GeolocalisationPage} from "../geolocalisation/geolocalisation";
import { PuzzlesPage } from '../puzzles/puzzles';
import { PhotoPage } from '../photo/photo';
import { QuizzPage } from '../quizz/quizz';
import { QrcodePage } from '../qrcode/qrcode';
import { NativeStorage } from '@ionic-native/native-storage';
import { Jeux } from '../../models/jeux-model';
import { TranslateService } from '@ngx-translate/core';
import { File } from '@ionic-native/file';

/**
 * Generated class for the QrcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  key_aventure:string;
  jeu:Jeux= new Jeux();
  jeux:Jeux[];
  jeuSuivant:Jeux;
  etat:any = false;
  joueur_key:any;

  title_class:any = {};
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public cpt : ComptageProvider,
    private nativeStorage: NativeStorage,
    public platform: Platform,
    public translate:TranslateService,
    public alertCtrl: AlertController,
    private file:File
    
    ) {
 
  }

  ionViewDidLoad() {


    this.jeu= new Jeux().deserialize(this.navParams.get("ceJeu"));
    this.jeux= this.navParams.get("jeux").map((a:Jeux) => new Jeux().deserialize(a));
    this.nativeStorage.setItem('dernierSequence'+this.jeu.aventures_id,{sequence:this.jeu.sequence});
   

    if(this.jeu.image_url){
      let img = this.jeu.image_url;
      
      let correctPath = img.substr(0, img.lastIndexOf('/') + 1); 
      let currentName = img.substring(img.lastIndexOf('/')+1);
      this.file.readAsDataURL(correctPath, currentName).then( dataurl =>{
                this.jeu.image_url = dataurl;

       //        let confirm = this.alertCtrl.create({
			    //     title: this.translate.currentLang == "fr" ?  this.jeu.titre.fr : this.jeu.titre.en,
			    //     message: (this.translate.currentLang == "fr" ?  this.jeu.description.fr : this.jeu.description.en)+"<br><hr><ion-thumbnail><img  src='"+dataurl+"'  style='max-height:200px; margin: auto;' /></ion-thumbnail>",
			    //     buttons: [
			       
			    //     {
			    //       text: this.translate.currentLang == "fr" ? "SUIVANT" : "NEXT",
			    //       handler: () => {
			    //         this.nextGame();

			    //       }
			    //     }
			    //     ],
			    //     cssClass: 'alert-geohunt',
			    //     enableBackdropDismiss: false
			    //   });
			    // confirm.present(); 


              }).catch(function(err){
                alert('Error '+JSON.stringify(err));
      })
    }
    console.log('Jeu: %o',this.jeu)
    console.log('Jeux: %o',this.jeux)
    console.log('JeuxSuivant: %o',this.jeuSuivant);

    // if(!this.jeu.image_url){
    // 	let confirm = this.alertCtrl.create({
			 //        title: this.translate.currentLang == "fr" ?  this.jeu.titre.fr : this.jeu.titre.en,
			 //        message: this.translate.currentLang == "fr" ?  this.jeu.description.fr : this.jeu.description.en,
			 //        buttons: [
			       
			 //        {
			 //          text: this.translate.currentLang == "fr" ? "SUIVANT" : "NEXT",
			 //          handler: () => {
			 //            this.nextGame();

			 //          }
			 //        }
			 //        ],
			 //        cssClass: 'alert-geohunt',
			 //        enableBackdropDismiss: false
			 //      });
			 //    confirm.present(); 
    // }
	    

   
 
  }

 

  nextGame(){

   

    this.jeux = this.jeux.sort(function(a:Jeux, b:Jeux){return Number(b.sequence) -Number(a.sequence)});
     console.log(this.jeux);
    let curSequence:Number =  0
  	for(var i=0;i<this.jeux.length;i++){
        curSequence = this.jeux[i].sequence;
  
        if(curSequence <= Number(this.jeu.sequence)) {
          this.jeuSuivant = (i!=0) ? this.jeux[i-1] : new Jeux();
          break;
        }else {
          this.jeuSuivant = new Jeux();
        }
    }
   	console.log(this.jeuSuivant);
    // alert(this.jeuSuivant.type);
    switch (this.jeuSuivant.type) {
     case "quizz":
        // code...
        this.navCtrl.push(QuizzPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "puzzle":
        // code...
        this.navCtrl.push(PuzzlesPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "geolocalisation":
        // code...
        this.navCtrl.push(GeolocalisationPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "qrcode":
        // code...
        this.navCtrl.push(QrcodePage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "photo":
        // code...
        this.navCtrl.push(PhotoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      case "info":
        // code...
        this.navCtrl.push(InfoPage,{jeux:this.jeux,ceJeu:this.jeuSuivant,joueur_key:this.joueur_key});
        break;
      default:
        // code...
        this.navCtrl.push(LeaderboardPage,{id_aventure:this.jeu.aventures_id,dernierJeu:this.jeu})
        break;
    }
    /*this.cpt.pauseTimer();
  	this.navCtrl.push(LeaderboardPage)*/
    //this.cpt.endTimerJeu("qrcode",1);
    
  	// this.navCtrl.push(LeaderboardPage)

  }



}
