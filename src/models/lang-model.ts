import { Deserializable } from "./deserializable-model";

export class Lang implements Deserializable{
	
	fr:string;
	en:string;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}

