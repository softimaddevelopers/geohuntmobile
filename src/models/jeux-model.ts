import { Deserializable } from "./deserializable-model";
import { Lang } from "./lang-model";

export class Jeux implements Deserializable{
	key:string;
	titre:Lang = new Lang();
	description:Lang = new Lang();
	image_url:string;
	type:string;
	longitude:number;
	latitude:number;
	question:Lang = new Lang();

	reponse:Lang = new Lang();
    format:Number
	texte_qrcode:string;
	requis:Boolean=false;
	indices:any[]=[];
	penalisation_passer:Number;
	sequence:Number;
	aventures_id:String;
	

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}