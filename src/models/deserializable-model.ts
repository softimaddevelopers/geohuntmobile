// src/app/shared/models/deserializable.model.ts

export interface Deserializable {
  deserialize(input: any): this;
}
export interface Indices{
	indice:string;
	temps_penalisation:number;
}
