import { Deserializable } from "./deserializable-model";
import { Lang } from "./lang-model";
export class Aventure implements Deserializable{
	key:string;
	titre:Lang = new Lang();
	description:Lang = new Lang();
	avatar_url:string="https://firebasestorage.googleapis.com/v0/b/geohunt-afa96.appspot.com/o/shiva.jpg?alt=media&token=7edd5c39-3478-4d59-9c39-20645aee97c5";
	avatar_local_url:string;
	temps_min:Number;
	temps_max:Number;
	distance:Number;
	prix:Number;
	niveau:string;
	lieu:string;
	status:string = "brouillon";
	transports:any= [];
	language:Lang = new Lang();;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}