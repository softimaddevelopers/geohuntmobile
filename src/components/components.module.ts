import { NgModule } from '@angular/core';
import { LayoutInfoComponent } from './layout-info/layout-info';
@NgModule({
	declarations: [LayoutInfoComponent],
	imports: [],
	exports: [LayoutInfoComponent]
})
export class ComponentsModule {}
