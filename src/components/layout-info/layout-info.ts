import { Component } from '@angular/core';

/**
 * Generated class for the LayoutInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'layout-info',
  templateUrl: 'layout-info.html'
})
export class LayoutInfoComponent {

  text: string;

  constructor() {
    console.log('Hello LayoutInfoComponent Component');
    this.text = 'Hello World';
  }

}
