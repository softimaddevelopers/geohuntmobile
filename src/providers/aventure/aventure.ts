import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
//import { Observable} from 'rxjs';
//import { Aventure } from "../../models/aventure-model";
import * as firebase from 'firebase/app';

/*
  Generated class for the AventureProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AventureProvider {
	task: firebase.storage.UploadTask;
	constructor(public db: AngularFireDatabase) {
		
	}

	getAventures(){
		return this.db.list("/aventures").snapshotChanges();
	}

	getData(path:string){
		return this.db.database.ref(path)
	}
	create(path:string,data:Object=null){
		return this.db.list(path).push(data)
	}
	update(path:string,id:string,data:Object){
		return this.db.list(path).update(id, data);
	}
	set(path:string,id:string,data:Object){
		return this.db.list(path).set(id, data);
	}
	delete(path:string,id:string){
		return this.db.list(path+'/'+id).remove();
	}
	pushFile(path :string ='/jeux',file:any){
		let sotrageRef = firebase.storage().ref();
		this.task = sotrageRef.child(path+'/image').put(file);
		return this.task;
	}

	get(path){
		return this.db.object(path);
	}

	list(path){
		return this.db.list(path).snapshotChanges();
	}



}
