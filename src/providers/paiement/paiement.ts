//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";
//import * as firebase from 'firebase';
/*
  Generated class for the PaiementProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PaiementProvider {

  constructor(
  	public db: AngularFireDatabase
  ) {
    console.log('Hello PaiementProvider Provider');
  }
  getData(path:string){
  	return this.db.database.ref(path)
  }
  create(path:string,data:Object=null){
  	 return this.db.list(path).push(data);
  }
  update(path:string,id:string,data:Object){
  	return this.db.list(path).update(id, data);
  }
  set(path:string,id:string,data:Object){
    return this.db.list(path).set(id, data);
  }
  delete(path:string,id:string){
  	return this.db.list(path+'/'+id).remove();
  }
}
