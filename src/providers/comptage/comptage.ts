//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AlertController } from 'ionic-angular';
//import { Observable } from 'rxjs/Observable';
//import 'rxjs/add/observable/interval';
//import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { NativeStorage } from '@ionic-native/native-storage';
import { TranslateService } from '@ngx-translate/core';
//import { Lang } from "../../models/lang-model";
import { Jeux } from '../../models/jeux-model';

@Injectable()
export class ComptageProvider {
  public display_time;
  public geoTimer;
  public pause;
  deb_tps : any;
  tps_en_cours : any;

  key_av:any;
  key_jeu: any;
  joueur_key:any;
  public numero_suivant:number;
  warning:any;
  indice_verifie:any=[];

  listes_indices:any;

  interval:any;


  start:Boolean = false;
  temps_penalisation:any;
  check :Boolean = false;
  jeu:Jeux = new Jeux();
  jeux:Jeux[];
  public nombres:any;
  fin:Boolean = false;
  constructor( public db: AngularFireDatabase, private nativeStorage: NativeStorage,
    public translate: TranslateService,
    public alertCtrl:AlertController) {

  }

  getAllJeux(jeux){
    this.jeux= jeux.map((a:Jeux) => new Jeux().deserialize(a));
  } 

  date_debut(key_aventure, joueur_key, date_debAv, date){
     
    this.key_av = key_aventure;
    this.joueur_key = joueur_key;
    if(date_debAv) {
      this.deb_tps = date_debAv;
      this.startTimer2();
    } else {
      this.nativeStorage.setItem("dateDeb"+key_aventure, date).then(()=>{
        this.deb_tps = date;
        this.startTimer2();
      });
    }
  }

  endTimerJeu(type,number,cle_jeu,aventures_id,penalisation_passer_jeu,etat){ /* ex: type = quizz, number = 1 */
    //alert("end Timer");
    this.fin = true;
    let that = this;
    // this.pause = this.display_time;
    this.key_jeu = cle_jeu;
    
    let fin_jeu = this.display_time;
    let data_jeu = { 
      type_jeu: type,
      temps: fin_jeu,
      skip:etat,
      indice:this.indice_verifie.length > 0 ? this.indice_verifie : ""
    }

    let action = 0;

    if(etat == true && action == 0){
      let date_actuel = this.deb_tps;
      this.newDateDeb(date_actuel,penalisation_passer_jeu,aventures_id).then(()=>{
          this.writeStorageEnd(aventures_id, cle_jeu, data_jeu);
      });
      action++;
    }else{
          this.writeStorageEnd(aventures_id, cle_jeu, data_jeu);
    }


       
  }

  writeStorageEnd(aventures_id, cle_jeu, data_jeu){

      let self = this;
     this.nativeStorage.getItem('joueurJeux').then((data:any)=>{
            // alert("aventures_id "+aventures_id);
            this.nativeStorage.getItem("paiement"+aventures_id).then((paie)=>{
                  data[''+this.key_av][''+paie.joueur_id].jeux = data[''+this.key_av][''+paie.joueur_id].jeux ? data[''+this.key_av][''+paie.joueur_id].jeux : {};
                  data[''+this.key_av][''+paie.joueur_id].jeux[cle_jeu] = {};
                  data[''+this.key_av][''+paie.joueur_id].jeux[cle_jeu] = data_jeu;
                  // data[''+this.key_av][''+this.joueur_key].jeux["date_deb_aventure"] = this.deb_tps;
                  data[''+this.key_av][''+paie.joueur_id]["fin_deb_aventure"] = Date.now();
                  let current = Date.now();
                  let temps = new Date(current - this.deb_tps);
                  let temps_seconds = /*1000**/Math.round((Math.abs(temps.getTime()))/1000);
                   // alert()
                   //data[''+this.key_av][''+paie.joueur_id]["temps_total_aventure"] = this.display_time;
                   //data[''+this.key_av][''+paie.joueur_id]["temps_total"] = temps_seconds;
                  //setTimeout(function(){
                    setTimeout(function(){

                        let endTime = self.display_time;
                        data[''+self.key_av][''+paie.joueur_id]["temps_total_aventure"] = endTime;
                        data[''+self.key_av][''+paie.joueur_id]["temps_total"] = temps_seconds; 
                        self.pause = endTime;
                        self.nativeStorage.setItem("temps_total_aventure"+self.key_av, endTime);

                        self.nativeStorage.setItem('joueurJeux', data).then((data1)=>{
                      
                         },(err)=>{
                           alert('Err :'+err)
                         });

                    }.bind(this),1000);
                    
                        

                
                   
            })
                  
        }, (err)=>{
              alert("Error storage : "+JSON.stringify(err)+" paiement"+aventures_id);
        });
  }
  newDateDeb(deb_tps,tps_penal,key_av){
    
    // let datey = new Date(deb_tps).getFullYear();
    // let datem = new Date(deb_tps).getMonth();
    // let dated = new Date(deb_tps).getDate();
    // let dateh = new Date(deb_tps).getHours();
    // let datemin = new Date(deb_tps).getMinutes();

    //let dateparse = new Date(datey,datem,dated,dateh,datemin,tps_penal);
    let newDatDebPenalIndice = new Date(this.deb_tps - (tps_penal*1000)).getTime();


    this.deb_tps = newDatDebPenalIndice;
    return this.nativeStorage.setItem("dateDeb"+key_av, newDatDebPenalIndice);

  }

  penalisation_indices(jeu,aventures_id,jeu_key){

    let fermer = "Fermer";
    //let acun_indice = "Aucune indice disponible!";
    let penal_indice = "Temps pénalisé";
    let accept = "ACCEPTER";
    let cancel = "ANNULER";
    let hint = "INDICE";

    let prevent_penalisation = "Si vous utilisez l'indice,vous serez pénalisé";

    //for(let indice in indices){
      //console.log("voici l'indice: "+indice);
      //console.log("le texte de l'indice: "+indices[indice].indice.fr);
      //if(indice == numero/*  && this.check == false*/){

        //this.indice_verifie.push(indice);
        this.temps_penalisation = jeu.indices[0].temps_penalisation;
        this.warning = jeu.indices[0].indice[this.translate.currentLang];
        //this.check = true;
       // break;
      //}
      //return false;
    //}

    //this.check = false;

    this.translate.get([fermer,penal_indice,prevent_penalisation, accept, cancel, hint]).subscribe(
      value => {

      let confirm = this.alertCtrl.create({
        title : value[prevent_penalisation] + " : "+this.temps_penalisation+" s",
        cssClass: 'alert-geohunt',
        buttons : [
          {
            text: value[cancel],
            role: 'cancel',
          },
          {
            text : value[accept],
            handler: () =>{
                  let indice = this.alertCtrl.create({
                    title: value[hint]+ " : "+ this.warning,
                    buttons: [
                    {
                      text: value[fermer],
                      handler: () => {

                        this.newDateDeb(this.deb_tps,this.temps_penalisation,aventures_id);
                        jeu.indices.shift();


                        this.nativeStorage.setItem('indicesPenalisations'+aventures_id+jeu_key,
                          {indices: jeu.indices}).then((data)=>{
                            // alert("update indice :"+JSON.stringify(data))
                        },(err)=>{ 
                          this.nativeStorage.setItem('indicesPenalisations'+aventures_id+jeu_key,{indices: jeu.indices}).then((success)=>{
                            //alert("update indice :"+JSON.stringify(new_indices))
                          });
                        });     
                      }
                    }
                    ],
                    cssClass: 'alert-geohunt'
                  });
                  indice.present(); 
            }  
          }

        ]
      });

      confirm.present();
                  
    });

  }



  finTimerJeu(){

 
    // setTimeout(function(){
    //   this.nativeStorage.setItem("temps_total_aventure"+this.key_av, this.display_time);
       
    // }.bind(this),1000)

  }

  getTimer(){

      if(this.deb_tps){
         
          let cur:any = new Date();
          let temps = Math.abs(cur - this.deb_tps);
 
          return this.secondsToHMS(temps/1000);
      }else{
        this.nativeStorage.getItem('joueurJeux').then((data) =>{
            this.deb_tps = data[this.key_av][this.joueur_key].date_deb_aventure;
            
            this.getTimer();
        })
        // return "00:00:00";
      }
      
  }

   secondsToHMS(secs) {
      
      var sign = secs < 0? '-':'';
      secs = Math.abs(secs);
      return sign +  this.z(secs/3600 |0) + ':' + this.z((secs%3600) / 60 |0) + ':' + this.z(secs%60);
    }

    z(n){return (n<10?'0':'') + parseInt(n);}



  startTimer2(){
        if(!this.start){
          let self = this;
              this.interval =  setInterval(function(){
                  self.display_time = self.getTimer();
                

              }.bind(this),1000);
              self.start = true;
        }
        
  }

}
